+++
title = "Key Dates"
description = "Important Deadlines for useR! 2021. The annual user conference of the R Language for Statistical Computing."
keywords = ["program","schedule","events","topics"]
+++


# Roadmap to useR! 2021

The below list contains key deadlines for useR! 2021 participants. 

**2020-12-15** Tutorial submission opens.

**2021-01-25** Abstract submission for regular talks, panels, incubators and elevator pitches opens.

**2021-02-05** Tutorial submission closes.

**2021-03-22** Abstract submission closes

**2021-03-12** Communicate selected tutorials.

**2021-04-20** Registration opens.

**2021-05-10** Communicate selected abstracts.

**2021-05-28** ~~Early Bird registration closes.~~ Early Bird rates are extended until Registration closes

**2021-06-23** Deadline for selected contributors to send us videos, technical notes and transcripts.

**2021-06-25** Registration closes.

**2021-07-05** Conference opens.

**2021-07-09** Conference ends.



<br>
<br>
<br>
<br>
