+++
title = "useR! 2021 Session Overview - Enter your session!"
description = "Join useR! 2021. useR! is the annual conference of the R Language for Statisical Computing. Meet R enthusiasts from more than 100 countries from Buenos Aires to Zurich, Nairobi and Sydney. "
keywords = ["Rstats","programming","conference","week of R"]
+++


**Note**:  All sessions time are **UTC**. For a more convenient timezone conversion see our <a href="/program/schedule">schedule</a>. The calendar has the ability to switch timezone for you (choose time zone on the bottom right).


## Monday, July 5

<table>
 <thead>
  <tr class="moo">
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Session </th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Zoom</th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Stream</th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">start </th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">end </th>
  </tr>
 </thead>
<tbody>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Welcome and Navigation Guide (PDT)</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/63257691462"><img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/vK38CaT9O9o"><img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">22:15 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">23:15 </td>
  </tr>
  <tr class="moo ">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Welcome and Navigation Guide (AWST)</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/61078131043"><img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/AxwRu3iJQ2M"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">07:15</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">08:15</td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Welcome and Navigation Guide (CEST) </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/68955381204"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/EVPhFdVc9CE"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">13:15</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">14:15</td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Keynote: R Spatial</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/64748155817"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/O8V4QJ13Gjc"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">15:30</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">16:30</td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">1A Community and Outreach 1</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/63120736702"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://www.youtube.com/watch?v=s1eXuWbmjUw"><img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">16:45</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">18:15</td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">1B R packages 1</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/63583650620"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/LXyruwSo2K0"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">16:45</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">18:15</td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">2A Data Management</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/63756697831"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://www.youtube.com/watch?v=7wtMiCtx3eo"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">18:30</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">20:00</td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">2B Shiny</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/61595956601"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/kIlcE9DWzZQ"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">18:30</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">20:00</td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Keynote: Tools and technologies for supporting algorithm fairness and inclusion</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/65224164863"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/v-7teTmTy0c"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">20:30</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">22:00</td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Incubator: Five principles to grow up your R community</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/63397609541?pwd=SXRhZ3lDdE1CWU4xN25EN1VWMzRNQT09"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">-</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">22:30</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">23:30</td>
  </tr>
</tbody>
</table>



## Tuesday, July 6

<table>
 <thead>
  <tr class="moo ">
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Session </th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Zoom</th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Stream</th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">start </th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">end </th>
  </tr>
 </thead>


  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Keynote: Can we do this in R?Answering questions about air quality one code at a time</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/64392325168"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/itIcbhPnAS8"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">07:30 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">08:30 </td>
  </tr>
  <tr class="moo ">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">3A Machine Learning and Data Management</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/63244580610"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/hUdCS7jCUIA"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">08:45 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">10:15 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">3BSpatial Analysis</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/67059906002"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/m_ORcYtoEDk"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">08:45 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">10:15 </td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">3C R Packages 2</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/64615857942"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://www.youtube.com/watch?v=NjM7sG1Lpc4"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">08:45 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">10:15 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">4A Trends, Markets and Models</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/64712430201"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/STMVoT9BkMs"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">11:15 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">12:45 </td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">4B Data viz and Spatial Applications</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/68589496073"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/qjEzzHx7YXA"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">11:15 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">12:45 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Keynote: Enseñando a enseñar sin perder a nadie en el caminoTeaching how to teach without leaving anyone behind</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/66133003881"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/AU5znXIP2F8"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">14:45 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">15:45 </td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Panel: R User or R Developer? This is the question!</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/66422070702"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/FjvMv-sXa8A"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">16:00 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">17:00 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Incubator: The role of the R community in the RSE movement</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/62369315869?pwd=WkRKTy8yMUlvMFdsMkhUOGlUOG0wUT09"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">-</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">16:00 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">17:00 </td>
  </tr>
</tbody>
</table>

## Wednesday, July 7 - Tutorial Day

Interactive Tutorials are open to participants for explicitly signed up for a tutorial. We will inform those who signed up in due time before their tutorial starts where to go. 
Several tutorials will be recorded and be made available after the conference if the format suits recording. 

## Thursday, July 8

<table>
 <thead>
  <tr class="moo">
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Session </th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Zoom</th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Stream</th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">start </th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">end </th>
  </tr>
 </thead>


  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Keynote: Expanding the Vocabulary of R Graphics</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/63343156387"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/AqHdKoUpq4A"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">00:30 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">01:30 </td>
  </tr>
  <tr class="moo ">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">5ATeaching R and R in Teaching</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/66251485188"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/KmxIJjSCKf8"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">01:45 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">03:15 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">5BMathematical/Statistical Methods</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/67250915835"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/A8QWvzNwaNk"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">01:45 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">03:15 </td>
  </tr>
  <tr class="moo ">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Incubator: Strategies to build a strong AsiaR Community </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/67879298517?pwd=eit6WGNQdnlhWWFjbnJ5TFY0QlFMUT09"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">-</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">03:30 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">04:30 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">6A Data Visualisation</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/68771581145"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/ThERcANKf6w"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">05:30 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">07:00 </td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">6B R in Production 1</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/66933977768"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/raVFhnOz1xw"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">05:30 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">07:00 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Keynote: Research software engineers and academia</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/68180851956"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/qTHykvYOItY"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">07:15 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">08:15 </td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">7A Ecology and Environmental Sciences</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/65760482214"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/Ey2bG_diVoA"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">09:15 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">10:45 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">7B Statistical modeling in R</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/64020205920"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/hDye4vD-4aY"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">09:15 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">10:45 </td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">7C Teaching, Automation and Reproducibility</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/68744654427"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://www.youtube.com/watch?v=dwSYUM-jMMw"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">09:15 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">10:45 </td>
  </tr>
  </tbody>
</table>

## Friday, July 9
<table>
 <thead>
  <tr class="moo">
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Session </th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Zoom</th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Stream</th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">start </th>
   <th style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">end </th>
  </tr>
 </thead>

  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Keynote: The R-universe project</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/68755807996"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/PPn-XRbDalc"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">12:30 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">13:30 </td>
  </tr>
  <tr class="moo ">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">8A Statistics and Bioinformatics</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/63552717074"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/-nI3wvX7pjI"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">13:45 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">15:15 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">8B R in Production 2</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/66331505297"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/aYV3bCu405k"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">13:45 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">15:15 </td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">8C Statistical modeling &amp;  Data Analysis 1</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/65998569862"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/OHBHTpQzFKE"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">13:45 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">15:15 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Keynote: Communicationelevating data analysis to make a real impact</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/63062949694"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/KyOWGyFgAa8"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">16:15 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">17:45 </td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Incubator: Expanding the R community in the Middle East and North Africa (MENA)</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/66890287093?pwd=QUM5ZFNWdEpwcHZOYVJ6YW9SK0Q2dz09"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">-</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">18:45 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">19:45 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Incubator: Stop reinventing the wheel: R package(s) for conference and abstract management</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/62369642087?pwd=NmZVNmdSV2Z0aTBuM0VGOEpTWHV0Zz09"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">-</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">18:45 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">19:45 </td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">9 AR in production 3</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/66178840635"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/-iMCx3f28kQ"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">20:00 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">21:30 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">9B Community and Outreach 2</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/67027065055"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/X7Ztn6meQHA"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">20:00 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">21:30 </td>
  </tr>
  <tr class="moo">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">9C Statistical modeling and data analysis 2</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/64752774089"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/75go99muWkM"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">20:00 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">21:30 </td>
  </tr>
  <tr class="moo session-odd">
   <td class="session-title" style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">Closing and Awards</td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://ethz.zoom.us/j/68860973600"> <img src="/img/artwork/zoom.svg" width=50></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;"><a href="https://youtu.be/-tStzackLYE"> <img src="/img/artwork/youtube.svg" width=80></a></td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">21:30 </td>
   <td style="text-align:left;padding-left:30px;padding-top:5px;padding-bottom:5px;padding-right:25px;">23:30 </td>
  </tr>
</tbody>
</table>

<br>
<br>
