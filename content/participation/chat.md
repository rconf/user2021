+++
title = "Guide to the useR! Lounge"
description = "Meet speakers, sponsors and other participants on our main chat platform. Join all events including social activities from here."
keywords = ["Lounge","Lobby", "useR! meeting point"]
+++


<a href="/chatGuide.pdf">Download a .pdf version of the below guide.</a>


### Connecting

To log in for the first time, go to <https://join.user2021.r-project.org/> and use your conftool **username** your conftool **user_id** as your initial password. 
After you logged in, please type */connect* to connect to our custom useR! IRC server.  

Google Chrome is the recommended browser for Mac VoiceOver users.

You will be received by a page like this:

![Screenshot of The Lounge interface showing a three-panel layout with a list of channels and private conversations to the left, the main conversation in the middle and a list of users in the current channel to the right. On top ist he name and topic of the current channel. On the bottom, the tex input where you type your messages. On the lower-left, there are two buttons: Settings and Help.](/img/artwork/thelounge-UI.jpg "The Lounge UI")

The first thing you need to do is change your password.
Go to Settings (<https://join.user2021.r-project.org/#/settings>) and towards the bottom you will see a section titled "Change password".

![Screenshot of the Settings page with a title that reads "Change password" and three text input with placeholders "Enter current password", "Enter desired new password" and "Repeat new password". On the bottom a button with text "Change Password".](/img/artwork/2021-06-15_15-09.png "Change password in settings")

## How to interact and navigate

You can interact with our chat both by clicking around or using commands and keyboard shortcuts.

You can see a list of all keyboard shortcuts by going to the Help section (available at <https://join.user2021.r-project.org/#/help>), but here are a list of the most relevant ones.

<table class="table" style="width: auto !important; margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:left;"> Keys </th>
   <th style="text-align:left;"> Action </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Alt-Down Arrow </td>
   <td style="text-align:left;"> Switch to the next window in the channel list. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Alt-Up Arrow </td>
   <td style="text-align:left;"> Switch to the previous windown in the channel list. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Alt-A </td>
   <td style="text-align:left;"> Switch to the first window with unread messages </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Alt-J </td>
   <td style="text-align:left;"> toggle jump to channel switcher </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Esc </td>
   <td style="text-align:left;"> close current contextual window(context menu, image viewertopic edit, etcand remove focus from input </td>
  </tr>
</tbody>
</table>

To write to a channel or private conversation you can start typing irregardless of where is your current focus; it is not necessary to navigate to the text entry box explicitly.

To send a private message to an user, click on their nick and then click on "Direct messages".
Alternatively, you can type `/query <nick> <message>`, where `<nick>` is the nick of the person you want to send a message to and `<message>` is the actual message.

To join a new channel, click on the plus icon to the left of "Main Entrance" and type the name of the channel on the text input that appears.
You can also type `/join <channel>` in any window.

You can navigate between channels you are currently in and opened private conversations by clicking on them on the left-hand side bar or by using the Alt + Up and Alt + Down keyboard shortcuts.
Finally you can search among those channels with the "Jump to..." text input that is above the Main Entrance and that can be focused with the Alt + J keyboard shortcut.

### marbot Commands

We've programmed a bot called marbot to help you and give you information during the conference, navigating the chat environment, setting preferences, and access conference schedule information.
You will mostly interact with the bot using commands that start with a "/".
Start any message with a "/" and you will get an autocompletion list of commands that you can navigate with the arrow keys.

You can send `/help` to any channel and you'll receive a private message from marbot with a help message.
You can also type `/commands` and the marbot will send you a list of the most useful commands with examples.

If this is your first time, we recommend you to type `/tutorial`.
The marbot will guide you through a step-by-step guide for customising your profile and learning some of the commands.

<table class="table" style="width: auto !important; margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:left;"> Command </th>
   <th style="text-align:left;"> Description </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> /help </td>
   <td style="text-align:left;"> Orientation to the useR! 2021 chat. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> /tutorial </td>
   <td style="text-align:left;"> A step-by-step introduction to our platform. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> /commands </td>
   <td style="text-align:left;"> Get a list maRbot commands. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> /whereami </td>
   <td style="text-align:left;"> Reports which channel you are in. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> /nick &lt;nick&gt; </td>
   <td style="text-align:left;"> Change your display name to &lt;nick&gt;. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> /pronouns &lt;nick&gt; </td>
   <td style="text-align:left;"> Get &lt;nick&gt;'s pronouns. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> /setpronouns &lt;pronouns&gt; </td>
   <td style="text-align:left;"> Set your own pronouns. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> /join &lt;channel&gt; </td>
   <td style="text-align:left;"> Join &lt;channel&gt;, where &lt;channel&gt; is a channel name like "lobby" or "announcements. Joining a non existent channel is the same as creating a channel." </td>
  </tr>
  <tr>
   <td style="text-align:left;"> /schedule </td>
   <td style="text-align:left;"> Find out what's going on now </td>
  </tr>
  <tr>
   <td style="text-align:left;"> /schedule &lt;search term&gt; </td>
   <td style="text-align:left;"> Get a list of talks and events that match &lt;search term&gt;. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> /jitsi &lt;nick1&gt; &lt;nick2&gt; ... </td>
   <td style="text-align:left;"> Start a Jitsi video chat with users listed after the command. You all will receive a private message from the marbot with a link. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> /channel &lt;nick1&gt; &lt;nick2&gt; ... </td>
   <td style="text-align:left;"> Create a secret channel with with users listed after the command. You all will receive a private message with the name of the channel. With this you can have group private conversations. </td>
  </tr>
</tbody>
</table>

### Official Chat Channels

Official chat channels are channels that start with "\#".
User-created channels channels, on the other hand, start with "\#\#".

This is a list of some useful official channels.

<table class="table" style="width: auto !important; margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:left;"> ChannelName </th>
   <th style="text-align:left;"> Description </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> _welcome </td>
   <td style="text-align:left;"> welcoming channel to The Lounge </td>
  </tr>
  <tr>
   <td style="text-align:left;"> announcements </td>
   <td style="text-align:left;"> announcements from the organizing team </td>
  </tr>
  <tr>
   <td style="text-align:left;"> lobby </td>
   <td style="text-align:left;"> main lobby channel for informal exchanges </td>
  </tr>
  <tr>
   <td style="text-align:left;"> maRbot </td>
   <td style="text-align:left;"> bot help channel </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sandbox </td>
   <td style="text-align:left;"> channel to play with functions </td>
  </tr>
  <tr>
   <td style="text-align:left;"> sponsor_name </td>
   <td style="text-align:left;"> channel dedicated to a sponsor </td>
  </tr>
  <tr>
   <td style="text-align:left;"> tutorial_name </td>
   <td style="text-align:left;"> actual name of the tutorial </td>
  </tr>
  <tr>
   <td style="text-align:left;"> keynote_short_name </td>
   <td style="text-align:left;"> title of the keynote and presenter(s) </td>
  </tr>
  <tr>
   <td style="text-align:left;"> talk_session_name </td>
   <td style="text-align:left;"> A short version of the session name. The channel name for each session is visible in <a href="https://www.conftool.org/user2021/sessions.php" target="_blank">conftool</a> and <a href="user2021.r-project.org/program/schedule">on the schedule on our website</a>. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> EP_number </td>
   <td style="text-align:left;"> Elevator pitch channel for [title of presentation][name of presenter] </td>
  </tr>
  <tr>
   <td style="text-align:left;"> elevator_pitches </td>
   <td style="text-align:left;"> Main elevator pitch channel - Where to find an overview of all Elevator Pitch Channels </td>
  </tr>
  <tr>
   <td style="text-align:left;"> panel_name </td>
   <td style="text-align:left;"> Panel: [name] - Panelists [names] </td>
  </tr>
  <tr>
   <td style="text-align:left;"> incubator_name </td>
   <td style="text-align:left;"> Incubator[name]Presenters[names] </td>
  </tr>
  <tr>
   <td style="text-align:left;"> R-Ladies </td>
   <td style="text-align:left;"> Chat space for people of underrepresented gender </td>
  </tr>
  <tr>
   <td style="text-align:left;"> MiR_minorities_in_R </td>
   <td style="text-align:left;"> Chat space for people from minority groups </td>
  </tr>
  <tr>
   <td style="text-align:left;"> AfricaR </td>
   <td style="text-align:left;"> Chat space for people with connections to Africa </td>
  </tr>
  <tr>
   <td style="text-align:left;"> LatinR </td>
   <td style="text-align:left;"> Chat space for people with connections to Latin America </td>
  </tr>
  <tr>
   <td style="text-align:left;"> new_to_useR! </td>
   <td style="text-align:left;"> Chat space for new people attending UseR! </td>
  </tr>
  <tr>
   <td style="text-align:left;"> social_event_movie_talk </td>
   <td style="text-align:left;"> Chat space to comment on the movie - only open on Wednesday </td>
  </tr>
  <tr>
   <td style="text-align:left;"> QA_Navigation </td>
   <td style="text-align:left;"> Chat space for Q&amp;A session on Welcome segment. </td>
  </tr>
  <tr>
   <td style="text-align:left;"> social_event_mixR </td>
   <td style="text-align:left;"> Chat space for mixR </td>
  </tr>
</tbody>
</table>
