+++
title = "Call for Tutorials"
description = "Got RStats skills and enjoy teaching? Submit a tutorial. "
keywords = ["tutorials","teaching"]
+++


## Call for Tutorials

**Tutorial Submissions for useR! has ended. Consider submitting a talk, elevator pitch or panel.**

In this announcement, we outline topics of interest, presentation guidelines, and important dates for proposal submission.

The tutorials will be for R users in all sectors, e.g., researchers, government officers, and industry representatives who focus on the applicability of R in practical settings and for all levels. 

The duration of each tutorial can vary between two to four hours, we encourage shorter length but we expect at least two hours. We strongly believe that similar tutorials have helped the R user community to be updated about the latest packages, concepts, and best practices. 

Since we have a global event this year, we will accept proposals for tutorials to be taught in _English, Spanish or French_. **If you plan to teach in Spanish or French, the proposal must still be submitted in English.** 

These tutorials will be organized in _multiple time zones_, so we request that you select a convenient time zone. We will later determine the timezone based on all other tutorials and attendees' interest to combine them into a session. The recordings of the tutorials will be shared online with the consent of the organizers, instructors, and attendees. We prefer tutorials to have hands-on exercises or examples to be carried out by the participants for their best interest.

The organization will offer a payment of USD 500 for those tutorials that are selected.

## Topics of interest

Suggested topics for this edition, although not exclusive, are the following:

* Environmetrics
* Epidemiology 
* Big data
* Spatial analysis and modeling
* Transportation
* Usage of Rcpp
* Introduction to artificial intelligence/machine learning, exploratory machine learning, and/or ethical artificial intelligence
* Non linear statistical modeling 
* Reproducibility and best practices
* Ethical web scraping 
* Imaging and signal processing 
* Database management 
* Docker and RStudio 
* Natural Language Processing 
* Shiny app development and best practices
* Predictive modeling time series forecasting
* R Packages: building packages, CRAN submission and package maintenance
* Teaching R and R in teaching.
* Visualization
* Using git with R

## Submission guidelines

Submissions must be sent using the Tutorial Submission platform following this link https://graphs.kof.ethz.ch/user-tutorials/ from **Monday, December 14, 2020** to **Friday, February 5, 2021**.

Please read the [code of conduct](https://user2021.r-project.org/participation/coc/) and the [accessibility guidelines for tutorials](https://user2021.r-project.org/participation/tutorial-access/) before submitting the tutorials. 

Submissions must be _written in English_ and include: 

* The title of the tutorial
* Abstract
* The broad topic it covers
* The learning goals
* Time zone preference and time slot (please mention at least three different time zones you are comfortable with)
* Length of the tutorial
* Language in which the tutorial can be taught.
* The intended audience and method of online engagement with the audience
* If there exists, a link to the Tutorial materials and/or web page.
* Prerequisites / requirements
* Material sharing (license), recording consent
* A brief biography of the instructors

## Important dates

* Tutorial submission start: **Monday, December 14, 2020**
* Tutorial submission deadline: **Friday, February 5, 2021**
* Tutorial acceptance notification: **Friday, March 12, 2021**

## Review Criteria

Each submission will be carefully evaluated by our Program Committee regarding the submission’s overall quality, research scope, and its appeal to a reasonable fraction of the R community.

The submission will also be checked for its technical content and the pedagogical proposal.
Final decisions will be based on the review committee, depending on conference attendees’ interest, topic relevance, and slot availability. 

The outcome of the review process will be declared on **Friday, March 12, 2021**.

<br><br>

**Tutorial Submissions for useR! has ended. Consider submitting a talk, elevator pitch or panel.**

<br><br><br>
