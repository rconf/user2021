---
title: "How GitHub Actions allows us to automate the integration and deployment of our techguides webpage"
subtitle: "[[Template](https://github.com/useRconf/templates). _Recommended total reading time: 5-7min_]"
author: ["Peter Schmid, Mirai Solutions GmbH"]
date: "2021-06-23"
output:
  html_document:
    keep_md: TRUE
---

<center><img src="../mirai-solutions.png" alt="Mirai Solutions Logo"></center>

## Abstract

In this technical note, I would like to show how Mirai Solutions is continuously expanding its [techguides](https://mirai-solutions.ch/techguides/) in a flexible way based on an automated continuous integration and deployment workflow using [GitHub Actions](https://github.com/features/actions).

We use these techguides to collaborate, collect and share knowledge about technical solutions that we deem worth to describe in some detail, in the form of [bookdown](https://pkgs.rstudio.com/bookdown/) chapters hosted on a GitHub repository. As several people may work on separate issues in parallel, we use a branching approach, and have a streamlined process for automatic publication upon merging of pull requests into `main` / `master`. Continuous integration and deployment (CI / CD) pipelines allow us to automate all the routine steps of building the bookdown website, testing the repository and deploying the updated techguides to GitHub Pages under [mirai-solutions.ch/techguides](https://mirai-solutions.ch/techguides/). We use GitHub Actions to take care of that. As it is fairly new and not yet trivial to set up, we hope that this technical note can help and inspire others to consider using CI / CD.

### Keywords

Continuous Integration (CI), Continuous Deployment (CD), Collaboration

## Steps that GitHub Actions automates for us

- Build the bookdown website.
- Run some sanity checks on the rendered pages (especially around broken / mistyped links) via [HTMLProofer](https://github.com/gjtorikian/html-proofer).
- Load the built website as an artifact to enable reviewing the rendered content without having to check out any code.
- Deploy the built website to GitHub Pages as soon as new commits are pushed to the main branch with a merged PR.

## Configuring GitHub Actions

All that is required to set up GitHub Actions is a GitHub repository and a [YAML](https://en.wikipedia.org/wiki/YAML) file in the directory `.github/workflows` inside your repository.

Some basic examples of GitHub Actions usage for R packages (including deployment to shinyapps.io if you are packaging a Shiny app) can be found on our [techguides](https://mirai-solutions.ch/techguides/github-actions.html#complete-workflow-files). They make use of pre-defined actions which can be plugged-in and combined in GitHub Actions workflows. I also recommend having a look at [r-lib/actions](https://github.com/r-lib/actions) and their [examples](https://github.com/r-lib/actions/tree/master/examples), which served as inspiration for our own workflows.

## GitHub Actions workflow steps of our techguides

I focus on the additional, specific steps that are required for the techguides in addition to what the basic examples linked above show (the complete workflow can be found in our [GitHub repository](https://github.com/miraisolutions/techguides/blob/master/.github/workflows/site.yaml)). For a general explanation of how GitHub Actions workflow files works, see also our [GitHub Actions chapter](https://mirai-solutions.ch/techguides/github-actions.html#github-actions).

### Set up pandoc

Thanks to the action [r-lib/actions/setup-pandoc](https://github.com/r-lib/actions/tree/master/setup-pandoc), we only need to include one line to set up pandoc:
```yml
      - uses: r-lib/actions/setup-pandoc@v1
```

### Build bookdown website

This step basically runs an R script which can be as simple as this (after installing R and package depencencies):
```yml
      - name: Build site
        run: |
          bookdown::render_book("index.Rmd", "bookdown::gitbook")
        shell: Rscript {0}
```
While the `|` character defining [block style](https://yaml-multiline.info/) YAML is not strictly necessary, it is rather useful, as it removes the need to escape special YAML characters in YAML, such as quotes.

### Upload built site artifact

```yml
      - name: Upload built site artifact
        uses: actions/upload-artifact@v1
        with:
          name: site
          path: _book
```
This step is using a pre-defined action from [actions/upload-artifact](https://github.com/actions/upload-artifact). We also need to provide the "_book" path, as that's the folder where our actual bookdown site is built.

### Test website

To test the website, we are using HTMLProofer, for which we can conveniently use a docker image in our workflow. HTMLProofer will make sure the rendered pages are well formed and, in particular, that all links are working.
```yml
      - name: Test website
        uses: docker://klakegg/html-proofer:3.18.8
        with:
          args: _book
```
We also need to provide the path to the actual bookdown site as an extra argument.

### Deploy to GitHub Pages

```yml
      - name: Deploy to GitHub Pages
        if: github.ref == 'refs/heads/master'
        uses: crazy-max/ghaction-github-pages@v2.2.0
        with:
          target_branch: gh-pages
          build_dir: _book
          jekyll: false
        env:
          GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
```
Notice the if statement in the beginning, which makes sure we only execute this step if the workflow was triggered on `main` / `master` (and all previous steps were successful). To actually deploy our bookdown webpage to GitHub Pages, we can use another pre-defined action, [crazy-max/ghaction-github-pages](https://github.com/crazy-max/ghaction-github-pages), which requires some arguments to know what to build and on which branch, as well as authentification to GitHub.

## Integration into GitHub Pages

With the GitHub Pages step defined in GitHub Actions, we can automate that the rendered Gitbook will be deployed to the branch "gh-pages" of our techguides repository and then served on the URL we defined for it. This needs to be configured on GitHub under `Settings > Pages` (see also the [official documentation](https://docs.github.com/en/pages/getting-started-with-github-pages/creating-a-github-pages-site)).

## Declarations

- Abbreviations: CI / CD (continuous integration and continuous deployment), PR (pull request), Rmd (R markdown), [YAML](https://en.wikipedia.org/wiki/YAML).
- Author contributions: [techguides contributors](https://github.com/miraisolutions/techguides/graphs/contributors)
- Acknowledgements / Sources: [GitHub Actions](https://docs.github.com/en/actions), [r-lib/actions](https://github.com/r-lib/actions), [bookdown](https://pkgs.rstudio.com/bookdown/), [HTMLProofer](https://github.com/gjtorikian/html-proofer), [crazy-max/ghaction-github-pages](https://github.com/crazy-max/ghaction-github-pages)

<br><br>