---
title: "Technical Note"
author:
- Dr. Matthias Trampisch, Boehringer Ingelheim (correspondence)^[matthias.trampisch@boehringer-ingelheim.com]
- Julia Igel, Boehringer Ingelheim^[julia.igel@boehringer-ingelheim.com]
- Andre Haugg, Boehringer Ingelheim^[andre.haugg@boehringer-ingelheim.com]
date: "2021-06-17"
output:
  html_document:
    keep_md: yes
  pdf_document: default
  word_document: default
subtitle: _Data Access and dynamic Visualization for Clinical Insights (DaVinci)_
---



<script type="text/javascript"
  src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

## Abstract
This technical note introduces the Boehringer Ingelheim initiative on Data Access and dynamic Visualization for Clinical Insights (DaVinci). It is named after Leonardo da Vinci, one of the most diversely talented individuals ever to have lived. The main objective of the DaVinci project is to reflect this diversity creating a modular framework based on the shiny, which enables end-users to have direct access to clinical data via advanced visualization during clinical development.

DaVinci consists of a collection of shiny-based modules to review, aggregate and visualize data to develop and deliver safe and effective treatments for patients. Based on harmonized data concepts (CDISC SDTM[^CDISC]/ADaM[^ADAM]/source data), DaVinci provides and maintains GCP[^GCP] compliant modules for data review and analysis, which can easily be combined and customized into trial-specific dashboards by the end-user.

This note  outlines the developed approach, including the developed modular manager and highly flexible, custom-designed modules which all lead to an individual and customizable app experience. Main advantages of this approach are that the individual modules can be validated separately and used flexible in a joint shiny application, which permits easy validation considering GCP and 21 CFR part 11[^CFR]. This approach also supports trial, project or substance specific needs to get the most value out of the data.

### Keywords

R package development, R Shiny webapp, Bioinformatics, Clinical Trial analysis, Modularization, Visualization, Statistics

## Technology

Based on a in-house developed _modulemanager_ framework, some simple lines of code spawn a shiny app with separate modules. Using [moduleServer()](https://shiny.rstudio.com/reference/shiny/1.6.0/moduleServer.html) and  [bslib](https://cran.r-project.org/web/packages/bslib/index.html) permits an easy set up the customized UI and [custom themes](https://rstudio.github.io/bslib/articles/bslib.html#custom)); even provides basic password protection for extra security if needed.

Advantages of this modular approach include:

- Simplify and speed up the creation of an app, while retaining most of the flexibility provided by Shiny
- Standard way of accessing the data (modulemanager organizes and serves data through a global filter module) 
- Modules provide flexible data in- and output options and can be used as needed as focus lies on data-structure and not the content of the analysis (i.e. a hierarchical table module can be used for displaying Adverse Events or Concomitant Therapies)
- URL parameters are incorporated to instantiate the app in or at any given state 
- App bookmarking allows to store the settings of an app for future review or to share it with another user in a given state
- Full reactivity between modules to pass relevant data across modules (i.e. from an entry in the Hierarchical Table Module to a Patient Profile Module via patient ids)
- Easy setup of tailored applications with minimal coding skills
- Decoupled UI and server functions that allow quickly changing the look and feel of the application i.e. for branding purposes

Future plans include to extend the module gallery and provide exportable code at runtime that can reproduce the outputs seen in the application.

Overall, this approach helps the module development effort in a standardized manner, everyone develops things the same way, and the sustainability of the project instead of having each app resolving the problems in its own way.


## Example call

The pseudo code below sets up an application with 4 entries using 3 different modules indicated in the brackets:

1. __Disposition Figure__ (_dvcd.modules::mod_disposition_)
2. __Patient Profile Listing__ (_dvcd.modules::mod_papo_)
3. __Adverse Event (AE) Summary table__ (_dvcd.modules::mod_hierarchy_table_)
4. __Concommitant Therapies (CM) Summary Table__ (_dvcd.modules::mod_hierarchy_table_)

Note that in the pseudo code below the AE and CM summary tables use the same module, but a different data source.

In addition, _modulemanager_ will - by default - also initialize a data loading module and a global filter.

### Pseudo code

```r
#########################
##### Loading Data ######
#########################
# (opt) load password protection file
credentials <- your_password_file # data frame containing user names and hashed passwords (MD5, SHA, ..)

# Loading trial data sets
data <- your_trial_data_sets # list of data frames; In this example, the list contains adsl, adae and adcm

###############################
##### Define Application ######
###############################
module_list <- list(
  "Disposition" = dvcd.modules::mod_disposition(
    dataset = "adsl",
    module_id = "mod_disp",
    selection = c("RANDFL", "ARM")
  ),
  "Patient Profile" = dvcd.modules::mod_papo(
    module_id = "mod_papo",
    dataset = c("adsl", "adae", "adcm"),
    dm_data = "adsl", 
    key = "USUBJID"
  ),
  "AE Hierarchy table" = dvcd.modules::mod_hierarchy_table(
    module_id = "mod_AEHT",
    dataset = "adae",
    total_N_dataset = "adsl",
    subject_ID = "USUBJID",
    hierarchy = c("AEBODSYS", "AEDECOD"),
    group = "ARM"
  ),
  "CM Hierarchy table" = dvcd.modules::mod_hierarchy_table(
    module_id = "mod_CMHT",
    dataset = "adcm",
    total_N_dataset = "adsl",
    subject_ID = "USUBJID",
    hierarchy = c("ATC3", "CMDECOD"),
    group = "ARM"
)

###############################
##### Launch application ######
###############################
modulemanager::run_app(
  title = "DaVinci: Modular App",
  data = data,
  module_list = module_list,
  filter_data = "adsl",
  secure = TRUE,
  credentials = credentials
)
```

### Screen shots
<img style="padding-bottom:25px;" src="../images/1_dispo.png" width=700 alt="Default view of the Disposition Figure with no global filtering">


<img style="padding-bottom:25px;" src="../images/2_dispo_filtered.png" width=700 alt="Disposition Figure with global filtering on continouus variable age, and added hierachy entry on the categorical age group">

<img style="padding-bottom:25px;" src="../images/3_ae_table.png" alt="Default view of the Advese Events Summary Table" width=700>

<img style="padding-bottom:25px;" src="../images/4_ae_table_category.png" width=700 alt="Advese Events Summary Table with an added hierachicaly entry on category sex">

## **Availability of supporting source code and requirements**
Currently in-house development, but open for collaboration and code exchange. Please contact the authors if you are interested in an active exchange.
 

## Declarations
The opinions expressed in this technical note are solely those of the presenter and not necessarily those of Boehringer Ingelheim. 
Boehringer Ingelheim does not guarantee the accuracy or reliability of the information provided herein.


### List of abbreviations
ADaM = 	Analysis Data Model
AE = Adverse Events  
CDISC = Clinical Data Interchange Standards Consortium
CFR = Code of Federal Regulations
CM = Concommitant Medications  
DaVinci = Data Access and dynamic Visualization for Clinical Insights  
GCP = Good Clinical Practice  
SDTM = Study Data Tabulation Model


### Acknowledgments

The authors would like thank the core DaVinci team for their dedication on this project. 

<br>

[^CDISC]: [STDM | CDISC](https://www.cdisc.org/standards/foundational/sdtm)
[^ADAM]: [ADaM | CDISC](https://www.cdisc.org/standards/foundational/adam)
[^GCP]: [ICH E6 (R2) Good clinical practice](https://www.ema.europa.eu/en/ich-e6-r2-good-clinical-practice)
[^CFR]: [CFR - Code of Federal Regulations Title 21, Part 11](https://www.accessdata.fda.gov/scripts/cdrh/cfdocs/cfcfr/CFRSearch.cfm?CFRPart=11)

<br>
<br>