---
title: "Partial Least Squares Regression for Beta Regression Models"
subtitle: "The plsRbeta package"

author: ["Frédéric Bertrand, Université de technologie de Troyes, orcid_id: 0000-0002-0837-8281", "Myriam Maumy, Université de technologie de Troyes, orcid_id: 0000-0002-4615-1512"]
#affiliations: ["useR", "R-Ladies"]
date: "2021-07-03"
output:
  html_document:
    keep_md: TRUE
---


{{< technotes "https://userconf.github.io/technotes/tn.html" >}}
