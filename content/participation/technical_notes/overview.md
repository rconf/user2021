+++
title = "Overview of Technical Notes"
description = "Pre-circulated technical descriptions to read in advance."
keywords = ["Elevator Pitches","Technical Notes", "Precirculated"]
+++


## What Are Technical Notes at useR! ? 

useR! has elevator pitches that allow presenters to quickly present their work and interact with useRs, discussing their work based on pre-circulated input. This input may come in the format of a technical note. Technical notes are brief technical description, beyond a mere abstract, but shorter than a whole paper or package documentation. The below list gives an overview of all submitted technical notes. 

- <a href="/participation/technical_notes/t108/technote/">Technical Note 108: Perform Chromatin Segmentation Analysis in R </a>
- <a href="/participation/technical_notes/t138/technote/">Technical Note 138: Partial Least Squares Regression for Beta Regression Models </a>
- <a href="/participation/technical_notes/t139/technote/">Technical Note 139: Statistical Workflows in R for Imaging Mass Spectrometry Data </a>
- <a href="/participation/technical_notes/t146/technote/">Technical Note 146: tramME: Transformation Models with Mixed Effects </a>
- <a href="/participation/technical_notes/t154/technote/">Technical Note 154: Bringing Auto-diff to R packages </a>
- <a href="/participation/technical_notes/t164/technote/">Technical Note 164: Network Reverse Engineering with Approximate Bayesian Computation</a>
- <a href="/participation/technical_notes/t186/technote/">Technical Note 186: Technical note on Cumulative Link Mixed Models (CLMMs) in R with the package 'ordinal' </a>
- <a href="/participation/technical_notes/t187/technote/">Technical Note 187: Navigating Insurance Claim Data through Tidymodels Universe </a>
- <a href="/participation/technical_notes/t208/technote/">Technical Note 208: Teaching Biology students to code smoothly with learnr and gradethis </a>
- <a href="/participation/technical_notes/t222/technote/">Technical Note 222: Fitting the beta distribution for the intra-apiary dynamics study of the infestation rate with Varroa Destructor in honey bee colonies </a>
- <a href="/participation/technical_notes/t233/technote/">Technical Note 233: Boehringer Ingelheim initiative on Data Access and dynamic Visualization for Clinical Insights (DaVinci) </a>
- <a href="/participation/technical_notes/t236/technote/">Technical Note 236: How GitHub Actions allows us to automate the integration and deployment of our techguides webpage</a>
- <a href="/participation/technical_notes/t257/technote/">Technical Note 257: Decision support using R and DataOps at a European Union bank regulator</a>

<br>
<br>

