---
title: Fitting the beta distribution for the intra-apiary dynamics study of the infestation rate with Varroa Destructor in honey bee colonies
author:
- Miotti, Camila$^1$
- Molineri, Ana$^1$
- Pacini, Adriana$^1$
- Orellano, Emanuel$^1$
- Signorini, Marcelo$^1$
- Giacobino, Agostina$^1$
- $^1$ Instituto de Investigacion de la Cadena Lactea ([CONICET-INTA])
date: "2021-06-30"
output:
  html_document: 
    highlight: tango
    keep_md: yes
    theme: journal
---




# $\color{coral}{\text{Abstract}}$

The parasitosis caused by the mite _Varroa destructor_ (Acari:Varroidae) has a negative impact in beekeeping production and colony survival. Furthermore, it has been shown that _V. destructor_ is a key vector for several viruses dissemination and therefore keeping mite populations below a critical threshold would prevent viruses epidemics.



The aim of this study was to estimate the infestation level of honey bee colonies with _Varrea destructor_ mites,  as a function of the autumn-winter parasitic dynamics. A total of six apiaries (with five colonies each) were assessed, each apiary had a "donor colony" infested with Varroa mites, and four uninfested "receiver colonies". The apiaries were distributed within a 30 km radius, with a minimum distance of 2 km between them. All colonies were set with sister queens, and the apiaries were balanced according to the adult bee population size. 


All the uninfested colonies were treated during autumn against _V. destructor_ with amitraz (Amivar 500®) to reduce to 0% their infestation level. Samples for diagnosing the phoretic Varroa infestation ( _PV_ ) were taken 45 days after treatment (middle May) and monthly from June to September. The _PV_ mite infestation (estimated as N° Varroa / N° bees) was evaluated as a function of the colonies' disposition (circular/ lineal-middle/ lineal-edge) and the initial _PV_ mite level of the donor colony. A generalized linear mixed model with Beta distribution and Logit link was fitted. 



### $\color{coral}{\text{Keywords}}$
_Varroa destructor, population dynamics, Apis mellifera, beta distribution, glmmTMB_


# $\color{coral}{\text{Materials & Methods}}$

<img src="Figura m&M.jpg" width="40%" style="float:left; padding:10px" />

### $\color{#EE6A50}{\text{Experimental conditions}}$


  * Two apiaries in a circular arrangement, each with a donor colony in the center of the arrangement 
  
  
  * Two apiaries in a linear arrangement, each with a donor colony located at the end of the line
  
  * Two apiaries in a linear arrangement, each with a donor colony located in the middle of the line.

===        


        
### $\color{#EE6A50}{\text{The model}}$

<img src="figura1.png" width="65%" style="float:right; padding:10px" />


  * The class of beta regression models are useful when the variable assume values in the standard unit interval (0,1), as PV mite infestation
  
   
  * The 'glmmTMB' package allows the fitting of generalized linear and linear mixed models with various extensions. Models are fitted using maximum likelihood estimation through 'TMB' (Template Model Builder).


  * Then, a generalized linear mixed model with Beta distribution and Logit link was fitted  using glmmTMB function (glmmTMB package), including the colony as random effect.


 
 

===

### $\color{#EE6A50}{\text{The final model}}$
  
$$
PV_i = \beta_{0} + \beta_1time^3 + \beta_2disposition + \beta_3initialPV*time + b_1colony_i + \epsilon_i 
$$

<img src="model effects.JPG" width="65%" />


# $\color{coral}{\text{Finding}}$

  * The colony disposition effect was statistically significant (p=0.0126)
  
  * The initial _PV_ mite level was statistically significant (p=0.0314)
  
  * Our results suggest that the PV temporal dynamics within each colony differ according to the initial _PV_ of the neighbor colonies, and that the infestation probability is higher for the lineal-middle disposition of the colonies.



# $\color{coral}{\text{Future}}$

  * To model temporal correlation of _PV_ within each colony

  * To study the re-infestation direction between donor and receiver colonies

  * To fit and additive model that includes colony, apiary and environment drivers explaining the temporal dynamics of _PV_ parameter


# $\color{coral}{\text{Author contributions}}$
AG and MS conceived the study; CM, EO, AM and AG prepared and evaluated colonies at field level. AP and AG evaluated colonies at laboratory level. CM, AG and MS performed the statistical analysis.  CM and AG wrote the note draft, which was revised and had its final version approved by all coauthors.

# $\color{coral}{\text{Acknowledgments}}$ 

Camila Miotti and Adriana Pacini are doctoral and post-doctoral fellows respectively; Agostina Giacobino, Ana Molineri, and Marcelo L. Signorini are a Research Career Members from the Consejo Nacional de Investigaciones Cientificas y Tecnicas (CONICET, Argentina).
We are grateful to these resources for our inspiration to draft this Rmd Technical Note template.

<img src="logo.png" style="position:absolute;buttom:10px;right:350px;" width="150px"/>
