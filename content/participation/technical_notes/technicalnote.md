---
title: "Technical Note"
subtitle: "[[Template](https://github.com/useRconf/templates). _Recommended total reading time: 4-5min_]"
author: ["Author1, Affiliation1", "Author2, Affiliation2"]
#affiliations: ["useR", "R-Ladies"]
date: "2021-06-03"
output:
  html_document:
    keep_md: TRUE
---



<script type="text/javascript"
  src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

## Abstract

[Short statement or paragraph summarizing the Technical Note. Up to 250 words.]

**Tips for structured abstract:** Background; Findings, the technical details of the method, how the method was performed and statistical tests used; Conclusions, brief summary and potential implications. Please minimize the use of abbreviations and do not cite references in the abstract.

### Keywords

R package development, R Shiny webapp, Bioinformatics

## Findings

The findings section can be broken into subsections with short informative headings. There is no maximum length for this section, but we encourage authors to be concise. If you include citations, you need to add them manually, but you can cross link them with this syntax (e.g., Allaire 2020)[^blogdown]

### Including Rmd subsections

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document.

### Including code

You can embed an R code chunk like this:


```r
summary(cars)
```

```
##      speed           dist       
##  Min.   : 4.0   Min.   :  2.00  
##  1st Qu.:12.0   1st Qu.: 26.00  
##  Median :15.0   Median : 36.00  
##  Mean   :15.4   Mean   : 42.98  
##  3rd Qu.:19.0   3rd Qu.: 56.00  
##  Max.   :25.0   Max.   :120.00
```

### Including math
You can write math expressions using LaTeX formatting:
$$
ABC = pqr_{sub} \circ logABC
$$

### Including plots

You can also embed plots, for example:


```r
plot(pressure)
```

{{<figure src="pressure-1.png" alt="alternative text please make it informative" caption="this is what this image shows, write it here or in the paragraph after the image as you prefer" width="300">}}

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.

## Methods

For Technical Notes, this section is specific for including any additional methods used in the manuscript, that are not part of the new work being described in the manuscript.


### **Availability of supporting source code and requirements**

List the following:

-   Project name: e.g. My bioinformatics project
-   Project home page: e.g. https://github.com/useRconf/templates
-   Operating system(s): e.g. Platform independent
-   Programming language: e.g. R
-   Other requirements: e.g. R 4.0.5 or higher
-   License: e.g. GNU GPL, FreeBSD etc.

### Data availability

## Declarations

### List of abbreviations

...

### Competing interests

...

### Funding

...

### Author contributions

Janani Ravi and Mohamed El Fodil Ihaddaden created the Rmd and CSS
templates, respectively.

### Acknowledgments

We are grateful to these resources for our inspiration to draft this Rmd
Technical Note template.

1.  [RMarkdown cookbook by Yihui Xie.](https://bookdown.org/yihui/rmarkdown-cookbook/)
2.  [GigaScience Technical note instructions. Oxford Academic.](https://academic.oup.com/gigascience/pages/technical_note)
3.  [ResearchGate: Writing a technical note](https://www.researchgate.net/publication/42807606_Writing_a_technical_note)
4.  [NIST Technical Note Template](https://www.overleaf.com/latex/templates/nist-technical-note-template/gjytbtqtpsmt)
5. [Miscellaneous Wisdom about R Markdown & Hugo Gained from Work on our Website]( https://ropensci.org/blog/2020/04/23/rmd-learnings/)


[^blogdown]:*Allaire JJ, Xie Y, McPherson J, et al.,* Rmakrkdown: Dynamic Documents for R., 2020;

<br><br><br>
