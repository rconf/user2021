---
title: "Bringing Auto-diff to R packages"
subtitle: ""
author: ["Michael Komodromos, Imperial College London"]
date: "2021-06-21"
bibliography: "refs.bib"
output:
  html_document:
    keep_md: TRUE
---


{{< technotes "https://userconf.github.io/technotes/technical_note.html" >}}

<br><br>