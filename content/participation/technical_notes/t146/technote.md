---
title: "tramME: Transformation Models with Mixed Effects"
#subtitle: "[[Template](https://github.com/useRconf/templates). _Recommended total reading time: 4-5min_]"
author: ["Bálint Tamási, Epidemiology, Biostatistics and Prevention Institute, University of Zurich", "Torsten Hothorn, Epidemiology, Biostatistics and Prevention Institute, University of Zurich"]
date: "2021-06-23"
output:
  html_document:
    keep_md: TRUE
---





{{< technotes "https://userconf.github.io/technotes/tramME.html" >}}