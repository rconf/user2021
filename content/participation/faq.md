+++
title = "FAQ"
description = "Frequently Asked Questions"
keywords = ["FAQ","Frequently Asked Questions"]
+++


## FAQs

- <a href=#register>How can I register for useR!2021?</a>    
- <a href=#fee> What are the Registration Fees?</a>    
- <a href=#income>How do I find out the Category of Income of My Country?</a>    
- <a href=#coc>Where can I find the useR!2021 Code of Conduct</a>?    
- <a href=#waiver>Can I apply for a Registration Fee Waiver? How?</a>    
- <a href=#financial>Can I Apply for Additional Financial Support?</a>    
- <a href=#tutorial>How do I Book a Tutorial?</a>    
- <a href=#tutorial_av>Which Tutorials Are Available?</a>    
- <a href=#tutorial_waiver>Are Tutorials Included in the Fee Waiver?</a>    
- <a href=#tutorial_rec>Will Tutorials be Recorded?</a>
- <a href=#access>Where Can I Find Out More About the Accessibility of useR! 2021?</a>


### <a name="register">How can I Register for useR!2021?</a>

You can register online on the <a href=https://user2021.r-project.org/participation/registration/ target="_blank">useR!2021 registration page </a>.
 

### <a name="fees">What Are the Registration Fees?</a> 

Even though this is a virtual conference, costs are associated with it. We determined our fee structure based on two important points:

- We want everyone to be able to participate!
- We would appreciate it if you pay what you can!

The Early Bird rates have now been extedend until registration closes. Fees are adapted to your country of residence. Additionally, fees are waived if your employer does not have funds to cover the conference fees. There is no need to convince anyone — you only have to tell us that you don’t have the supporting funds. The _academia_ rate also applies to _non-profit organizations and government employees_, and the _student_ rate also applies to _retired people_. Freelancers are encouraged to select the rate they feel applies best to them. If you are from a _“Low Income”_ country, your fees are automatically waived.
 
### <a name=income>How Do I Find Out the Category of Income Applies to My Country?</a>
 
To find out more about income categories and our cost conversion method, visit our <a href=https://user2021.r-project.org/participation/registration-details/ target="_blank">Registration Details</a> page and our interactive table.

### <a name=coc>Where Can I Find the useR!2021 Code of Conduct?</a> 

Please find the <a href=https://user2021.r-project.org/participation/coc/ target="_blank">Code of Conduct</a> in the participation menu of our website. We will post links to it frequently and in several key locations on the conference website, e.g., the registration page. 
 

 
### <a name=waiver>Can I apply for a registration fee waiver? How?</a>

Yes, you can! 

We want to offer the chance to attend to everybody, including those who do not have any institutional funding. If you have funding support (from your unit/department/company/etc.), please use it to support your conference fee -- this will fund the current global R conference and the continued development of R. Please request a waiver only if you truly need it. 

Also, we are being more inclusive and accommodating due to the COVID-19 situation that is hurting some regions more than others. Make sure to reach out to seek financial assistance when needed.

You can request the fee waiver when you register: in ‘Step 2: Event and Item Selection,’ you will find a checkbox called ‘Fee Waiver’ that you should check and briefly state why you need a fee waiver in the text box below that checkbox. In ‘Step 3: Payment Details’ in the Method of Payment section, you have to choose ‘I have applied for a fee waiver and am waiting for the response.’ After you complete the next step, you finish your registration. You will get an additional email, usually within two weeks, that your waiver has been processed.

### <a name=financial>Can I apply for additional financial support?</a>

Yes, you can!

To request additional financial support, please follow these instructions during your registration: in ‘Step 2: Event and Item Selection,’ you will find a checkbox called ‘Specific Support to Attend the Conference’ that you should check and let us know what we can do to enable you to participate in the conference in good conditions in the text box below that checkbox. After you complete the next step, you finish your registration.
 
### <a name=tutorial>How do I book a tutorial?</a> 

Each participant can register for one or two tutorials. In the <a href=https://user2021.r-project.org/participation/registration/ target="_blank">registration form</a> you have to choose which tutorials you want to attend. Most tutorials can accommodate a limited number of users to ensure adequate support of the participants. If the tutorials have limited spots, we will inform the participants in advance to switch to alternate slots/topics.
 
### <a name=tutorial_av>Which tutorials are available?</a>

Since we have a global event this year, we will have a full day of tutorials in four tracks and different time zones. The 22 selected tutorials will be taught in different languages (English, Spanish or French), and they are aimed at beginner, intermediate and advanced users. Please find more information at <a href="https://user2021.r-project.org/program/tutorials/" target="blank">Tutorial page</a>. 
 
### <a name=tutorial_waiver>Are tutorials included in the fee waiver?</a>

Yes, but tutorial spots with waivers are limited so you are asked to limit yourself to one tutorial. Please select a tutorial of your choice during registration. We will confirm the Tutorial along with the fee waiver.
 
### <a name=tutorial_rec>Will Tutorials Be Recorded?</a>

For a few tutorials, we have the permission of the instructors to record the live sessions - these recordings will be made available after the conference on the, <a href="https://www.youtube.com/channel/UC_R5smHVXRYGhZYDJsnXTwg" target="_blank">RConsortium YouTube channel</a>. We will notify the participants at the start of the recording to turn off their video/audio based on their preference. Remember that the learning experience will be better (and far more effective) if you attend the live tutorial rather than watching a recording.

### <a name=access>Where Can I Find Out More About the Accessibility of useR! 2021?</a>

You can go to our <a href="https://user2021.r-project.org/participation/accessibility/" target="_blank">General Accessibility Guidelines on the site</a> and to the blog post <a href="https://user2021.r-project.org/blog/2021/02/17/preparing-for-an-accessible-conference/" target="_blank">Preparing for an Accessible Conference</a> 


<br><br><br>
