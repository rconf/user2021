+++
title = "Registration"
description = "Infos about Registration and Conference fees"
keywords = ["Fees","Registration"]
+++


## Registration closed. Thanks to all participants.

<br><br><br>

<br>


## Conference Fees

Even though this is a virtual conference, costs are associated with it. Our fee system was selected considering two important points: 

- We want everyone to be able to participate! 
- We appreciate if you pay what you can! 

The fees are adapted to the country where you live. Additionally, fees are waived if your employer doesn't have funds to cover the conference fees. There is no need to convince anyone, just tell us that you don't have the money. The _Academia_ rate applies also to non-profit organizations and government employees and the "student" rate applies also to retired people. Freelancers are encouraged to select the rate they feel applies to them.

If you are from a Low Income country, your fees are automatically waived. 

If you want to know the category of your country or learn more about the cost conversion method, visit our 
<a href="https://user2021.r-project.org/participation/registration-details/">**Registration Details**</a> page.
<br><br><br>

## Early Bird Fee 
### Conference 

**Early Bird rates now apply until Registration closes (June 25th)**

{{<table "table table-striped table-bordered">}}
|          | High Income Country | Higher Middle Income Country | Lower Middle Income Country | Low Income Country |
|----------|---------------------|------------------------------|-----------------------------|--------------------|
| Industry | $75.00              | $25.50                       | $10.50                      | waived             |
| Academia | $50.00              | $17.00                       | $7.00                       | waived             |
| Student  | $25.00              | $8.50                        | $3.50                       | waived             |
{{</table>}}
<br>

### Tutorials

The basic price is for two tutorials. If you book only one tutorial, you will get a 50% discount. <br>

{{<table "table table-striped table-bordered">}}
|          | High Income Country | Higher Middle Income Country | Lower Middle Income Country | Low Income Country |
|----------|---------------------|------------------------------|-----------------------------|--------------------|
| Industry | $50.00              | $17                       | $7                       | waived             |
| Academia | $30.00              | $11                       | $5                       | waived             |
| Student  | $15.00              | $6                        | $3                       | waived             |
{{</table>}}
<br>


<br><br><br>
