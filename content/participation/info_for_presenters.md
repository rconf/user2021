+++
title = "Information for Presenters"
description = "Guidelines and standards for presenters at useR! 2021"
keywords = ["Tutorials","Accessibility", "Presentations"]
+++

# Elevator Pitches

## Technical notes

If you have selected the technical note as format, you may now download the template <a href="https://gitlab.com/rconf/user-tech-note-template" target="blank">here</a>. We will not use the html file, but directly publish the markdown file on this website, in a dedicated section. You may have a look here: **[Technical Note Example](/participation/technical_notes/technicalnote)**.


During the Elevator Pitch Session, you will have the opportunity to interact with people interested in your work. The technical note should give enough information to start a meaningful discussion, without being too overwhelming. Reading time should be about 5 minutes. 

Please also consult our **[accessibility guidelines for technical notes](/participation/technical-access)**


# Recording Regular and Lightning Talks

## Technical requirements

**Length**

  *   Regular talks: 15 minutes
  *   Lightning Talk: 5 minutes

__Your face should be clearly visible__:

  * A video with yourself and your presentation material is more engaging and deaf or hard of hearing people may be able to read your lips.
  * You can use either a picture-in-picture layout (slides in the background, smaller speaker in the foreground), or slides and speaker splitting the screen.
  *   We suggest the following tools:
  
      *   Zoom (share your slides and record yourself)
      *   Kazam
      *   Simple Screen Recorder
      *   OBS Studio




**Submit your video in one of these formats:**
    
  *   .MOV
  *   .MPEG4
  *   .MP4
  *   .AVI
  *   .WMV
  *   .FLV

__Other recommendations__

* Feel free to increase the font size, color, and change the proportion of your slides to suit your screen and make sure that the text size is large enough. 
* Try to have a neutral background, and no ambient or background noise.
* Speak clearly and at a pace that allows for people to follow the content.
* Describe your graphics, pictures, videos, and memes.
* Speak every word on a slide, read long excerpts aloud.
* We do not recommend using the xaringanExtra slide tones in their current implementation. They produce a sound for every click, so in incremental mode you can get many clicks per slide, and also when navigating back during the presentation.




<br><br><br>
