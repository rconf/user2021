+++
title = "Code of Conduct"
description = "Unacceptable behavior in conference spaces. Procedure for Reporting CoC Violations."
keywords = ["Code of Conduct","CoC"]
+++


The organizers of useR! 2021 are dedicated to providing a safe and inclusive conference experience for everyone regardless of age, gender, sexual orientation, disability, physical appearance, race, or religion (or lack thereof).

All participants (including organizers, attendees, presenters, sponsors, and volunteers) at useR! 2021 are required to agree to the following code of conduct.

The code of conduct applies to all conference activities including talks, panels, workshops, and social events. It extends to conference-specific exchanges on social media, for instance, posts tagged with the identifier of the conference (e.g. #useR2021 on Twitter), replies to such posts, and replies to official social media accounts (e.g. [@_useRconf](https://twitter.com/_useRconf) on Twitter).

Organizers will enforce this code throughout and expect cooperation in ensuring a safe environment for all.

## Expected Behaviour

All conference participants agree to:

 - Be considerate in language and actions, and respect the boundaries of fellow participants.
 - Refrain from demeaning, discriminatory, or harassing behaviour and language.
 - Alert a member of the __Code Of Conduct Response Team__ if you observe violations 
of this code of conduct.

## Unacceptable Behavior

Behavior that is unacceptable includes, but is not limited to:

 - Stalking, e.g. persistent unwanted direct messages, sending unwanted pictures or malware links, logging online activity for harassment purposes.
 - Deliberate intimidation.
 - Sharing private conversations and discussions (e.g., screenshots of discussion channels, direct messages, etc), without consent.
 - Sustained or willful disruption of talks or other conference activities, including online discussion.
 - Use of sexual or discriminatory imagery, comments, or jokes.
 - Offensive comments related to age, gender, sexual orientation, disability, race, religion, nationality, or native language.
 - Inappropriate simulated physical contact, e.g. textual descriptions like “hug” or “backrub” or emoji representing such contact, used without consent.
 - Unwelcome sexual attention, which can include inappropriate questions of a sexual nature, asking for sexual favors, or repeatedly asking for dates or contact information.

If you are asked to stop unacceptable behavior you should stop immediately, even if your behavior was meant to be friendly or a joke.

Attendees who behave in an inappropriate manner are subject to actions listed under [__Responses to Code of Conduct Violations__](#responses-to-code-of-conduct-violations).

#### Additional Requirements for Conference Contributions

Presentations should not contain offensive or sexualised material. If this material is impossible to avoid given the topic (for example text mining of material from hate sites), the existence of this material should be noted in the abstract and, in the case of oral contributions, at the start of the talk or session.

#### Additional Requirements for Sponsors and Social Events Contractors

Sponsors and social event contractors should not use sexualised images, activities, or other material. They should not use sexualised clothing/uniforms/costumes, or otherwise create a sexualised virtual environment. In case of violations, sanctions may be applied, without the return of sponsorship contribution.

## Responses to Code of Conduct Violations

The Code of Conduct response team reserves the right to determine the appropriate response for all code of conduct violations. Potential responses include:

 - ban or mute from the conference spaces in order to stop the inappropriate behavior;
 - a formal warning to stop the harassing behavior;
 - expulsion from the conference;
 - removal of sponsor displays or conference presentations, or
 - cancellation or early termination of talks or other contributions to the program.

Refunds may not be given in case of expulsion.

## Acknowledgements

Parts of the above text are licensed CC BY-SA 4.0. Credit to SRCCON. This code of conduct was based on that developed for useR! 2018 which was a revision of the code of conduct used at previous useR!s and also drew from rOpenSci’s code of conduct.

<br><br><br>