+++
title = "Host & Institutional Support"
description = "The KOF Swiss Economic Institute at ETH Zurich hosts useR!2021. INTA (Argentina) provides institutional support."
keywords = ["host","global","virtual"]
+++

<div class="row">
<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/kof-bw.png" width="200px">


</div>
<div class="col-md-8">

<h2 id="kof-eth-zurich">KOF Swiss Economic Institute, ETH Zurich (Host / Global Digital Partner)</h2>

KOF provides expert information in the field of economic and business cycle research. It produces a multitude of forecasts and indicators which are used to monitor the economy. Its data pool of company surveys is unique throughout Switzerland. KOF uses this data to generate various indicators (such as the KOF Economic Barometer, the KOF Employment Indicator and the KOF Business Situation Indicator), which reflect Swiss economic sentiment. It evaluates its survey findings on a quarterly basis and presents them – as well as its economic forecasts – to the public. Furthermore, it analyses the innovation activities of Swiss companies, publishes studies on the labour market and healthcare spending, and comments on the latest economic developments. KOF – together with the Munich-​based ifo Institute – is also involved in the Joint Economic Forecast for Germany, which the German government uses as a benchmark for the performance of the economic development.

<h4 id="contact-kof-eth">Contact KOF Swiss Economic Institute</h4>
You can find more information about KOF on <a href="https://kof.ethz.ch" target="_blank">https://kof.ethz.ch</a> or follow the <a href="https://twitter.com/kofeth" target="_blank">German</a> and <a href="https://twitter.com/KOFETH_en" target="_blank">English</a> speaking twitter accounts.


</div>
</div>



<div class="row">
<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/inta.png" width="200px">


</div>
<div class="col-md-8">

<h2 id="INTA">National Institute of Agricultural Technology (INTA, Argentina)</h2>


We contribute to the sustainable development of the agricultural, agri-food and agro-industrial sector through research and extension. We promote innovation and knowledge transfer for the growth of the country.  INTA’s structure in Argentina is composed by 15 Regional Centres, six Research Centres (“Agro-industry”, “Natural Resources”, “Family Farming”, “Agricultural Research”, “Political, Economical and Social Sciences”, and “Veterinary and Agronomical Sciences”), 53 experimental stations, and more than 300 extension units. In addition, there are two private entities: Intea S.A. and ArgenINTA Foundation.  We focus on _Plant Production_ (extensive farming, intensive agriculture, industrial crops, forestry, fruit, vegetables, flowers and ornamental plants, organic production, fertilizers and agrochemicals, plant protection), _Animal Production_ (beekeeping, cattle, dairy, swine, sheep, goats, camels, pastures and natural vegetation, animal health), _Economic and Social Development_ (family farming, food production, rural development, and public policies), _Technological Development_ (biotechnology, bio-energy, innovation, agricultural machinery, value added) and _Environment_ (climate and water, environmental management, soils).

<h4 id="contact-inta">Contact</h4>
You can find more information in <a href="https://www.argentina.gob.ar/inta" target="_blank">our website</a> and follow us on <a href="https://twitter.com/intaargentina" target="_blank">Twitter</a> .
</div>
</div>


<div class="row">
<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/wsl.png" width="120px">


</div>
<div class="col-md-8">

<h2 id="WSL">The Swiss Federal Institute for Forest Snow and Landscape Research WSL</h2>


Research for People and the Environment: The Swiss Federal Institute for Forest Snow and Landscape Research WSL monitors and studies forest, landscape, biodiversity, natural hazards and snow and ice. WSL is a research institute of the Swiss Confederation and part of the ETH Domain. The WSL Institute for Snow and Avalanche Research SLF is part of the WSL since 1989.


</div>
</div>



<br><br><br>
