+++
title = "About"
description = "What's happening at the event"
keywords = ["program","schedule","events","topics"]
+++

# The useR! Conference

useR! conferences are non-profit conferences organized by community volunteers for the community, supported by the R Foundation. Attendees include R developers and users who are data scientists, business intelligence specialists, analysts, statisticians from academia and industry, and students. The useR! 2021 conference will be the first R conference that is global by design, both in audience and leadership. Leveraging a diversity of experiences and backgrounds helps us to make the conference accessible and inclusive in as many ways as possible and to grow the global community of R users giving new talents access to this amazing ecosystem. Being virtual makes the conference more accessible to minoritized individuals and we strive to leverage that potential. We pay special attention to the needs of people with a disability to ensure that they can attend and contribute to the conference as conveniently as possible.
Going fully virtual and global allows us to reimagine what an R conference can offer to presenters and attendees from across the globe and from diverse backgrounds. 

<img src="/img/past_useR/all_images-01.png" alt="A collection of photos taken from past useR conferences" width=500>

<small>
Photos Credits: Dale Maschette @Dale_Masch
Romain François @romain_francois
Derek Sollberger @DerekSollberger
R-Ladies Melbourne Inc @RLadiesMelb
Hiroaki Yutani @yutannihilat_en
Mitchell O'Hara-Wild @mitchoharawild
Gergely Daróczi @daroczig
Alimi Eyitayo @alimieyitayo
Africa R Users @AfricaRUsers
Angela Li @CivicAngela
R en Buenos Aires @renbaires
Ian Lyttle @ijlyttle
Erin LeDell @ledell
R-Ladies Global @RLadiesGlobal
erika siregar @erikaris
</small>

<br><br><br>
