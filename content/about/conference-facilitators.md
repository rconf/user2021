+++
title = "Conference facilitators"
description = "Information about team that facilitated the running of sessions at the useR! 2021 conference"
keywords = ["about","organisers","staff","R conference"]
+++

The following people played roles during the useR! 2021 dress rehearsals and during the conference days as Zoom hosts, session chairs and maRmots (general conference helpers). They are listed in alphabetical order.


## Session Chairs
{{<table "table table-striped table-bordered" "width:60%">}}
| Name                 |
|:---------------------|
|Adithi Upadhya|
|Balasubramanian Narasimhan|
|Christopher Maronga|
|Dorothea Hug-Peter|
|Earo Wang|
|Emi Tanaka|
|Erin Le Dell|
|Federico Marini|
|Heather Turner|
|Inger Fabris-Rotelli|
|Jenny Bryan|
|Joyce Robbins|
|Juan Pablo Ruiz Nicolini|
|Karthik Raman|
|Leo Collado-Torres|
|Liz Hare|
|Luis Verde|
|Maëlle Salmon|
|Marcela Alfaro|
|Mohamed El Fodil Ihaddaden|
|Natalia Morandeira|
|Peter Macharia|
|Saranjeet Kaur|
|Sevvandi Kandanaarachchi|
|Shel Kariuki|
|Stefanie Butland|
|Ulfah Mardhiah|
|Vebash Naido|
|Young Suk-Lee|
|Zhian Kamvar|
{{</table>}}

## Zoom Host

{{<table "table table-striped table-bordered" "width:60%">}}
| Name                 |
|:---------------------|
|Aditi Shah |
|Adrian Maga|
|Andrea Sánchez-Tapia|
|Ben Ubah|
|Cristal Rivera|
|David Yabar|
|Dorothea Hug Peter|
|Erin West|
|Faith Musili|
|Grecia|
|gwynn S.|
|Heather Turner|
|Joselyn Chávez|
|Juan Pablo Narváez-Gómez|
|Jyoti Bhogal|
|Linda Cabrera|
|Lucy Njoki Njuki|
|Luis Lau|
|Marcela Alfaro Córdoba|
|Margaret Kinyanjui|
|Maryam Alizadeh|
|Matt Bannert|
|Nasrin Fathollahzadeh Attar|
|Natalia Morandeira|
|Nicholas Spyrison|
|Olgun Aydin|
|Pamela E. Pairo|
|Peter Mwambura|
|Priyanka Gagneja|
|Rachel Heyard|
|Rafael López V.|
|Ricardo Murillo|
|Rita Giordano|
|Rocío Joo|
|Saranjeet Kaur|
|Stephen Balogun|
|Teresa Ortiz|
|Tuli Amutenya|
|Wencheng Lau-Medrano|
|Yanina Bellini Saibene|
|Yuya Matsumura |
{{</table>}}

## maRmots

{{<table "table table-striped table-bordered" "width:60%">}}
| Name                 |
|:---------------------|
|Claudia Huaylla|
|Cristal Rivera|
|Folajimi Aroloye|
|Gergely Daroczi|
|Heather Turner|
|Julio Viales|
|Kewalin Samart|
|Lucy Njoki Njuki|
|Mouna Belaid|
|Muriel Buri|
|Olgun Aydin|
|Paola Corrales|
|Rachel Wilkerson|
|Rick Pack|
|Rita Giordano|
|Russ Hyde|
|Sheila Saia|
|Stephen Balogun|
|Teresa Ortiz|
{{</table>}}

<br>
<br>
<br>
