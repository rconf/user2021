+++
title = "Sponsors"
description = "We thank our sponsors for their generous support of the R Community and the useR! conference."
keywords = ["community","sponsors", "useR!","support"]
+++


<div class="row">


<h2 id="platinum-sponsors">Platinum Sponsors</h2>

<p>Click their logo to meet our Platinum Sponsors on their useR! 2021 sites.</p>

<div class="col-md-12">
<a href="/about/platinum/appsilon/"><img style="padding:20px" src="/img/artwork/appsilon.svg" width="300px"></a>
<a href="/about/platinum/rstudio/"><img style="padding:20px" src="/img/artwork/rstudio-flat.svg" width="300px"></a>  
<a href="/about/platinum/roche/"><img style="padding:20px" src="/img/artwork/roche.svg" width="250px"></a>  
</div>

</div>

<div class="row">
<h2 id="gold-sponsors">Gold Sponsors</h2>

<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/ixpantia.png" width="200px">
</div>

<div class="col-md-8">
    <h3>ixpantia</h3>
    At ixpantia our passionate and experienced data scientists and business consultants work with organizations around the world to empower them to innovate with a data-driven approach. From our headquarters in Latin America we support our customers and help their teams develop data products and deploy them to production. <br><br>


<a  href="https://www.ixpantia.com/" target="_blank">**Visit company website**</a>


</div>


<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/cynkra.png" width="200px">
</div>

<div class="col-md-8">
    <h3>cynkra</h3>
    cynkra is a Zurich-based data consultancy with a strong focus on R. Established in 2018 by Kirill Müller and Christoph Sax, we help public and private organizations understand their data, optimize their workflows, streamline their modelling, and polish their data communication. We implement scripts, packages, and custom reports tailored to business needs and that seamlessly interface with existing infrastructure. We also set up and maintain customized R and RStudio infrastructure. Alongside our technical services, cynkra also offers on-site consulting and workshops on R and the tidyverse throughout Switzerland. The workshops cover key aspects of R usage, from basic data handling to interactive visualization. Cynkra is a passionate supporter of open source software and the R community. Our team members lead the development for several open source packages and contribute to many more. <br><br>


<a  href="https://www.cynkra.com/" target="_blank">**Visit company website**</a>


</div>


<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/iisa.jpg" width="200px">
</div>

<div class="col-md-8">
    <h3>IISA</h3>
    The International Indian Statistical Association (IISA) is a non-profit organization. Its objectives include promoting education, research and application of statistics and probability throughout the world with a special emphasis on the Indian subcontinent, fostering the exchange of information and scholarly activities between various countries as well as among other national/international organizations for the development of statistical science, serving the needs of young statisticians and encouraging cooperative efforts among its members in education, research, industry and business. <br><br>


<a  href="https://www.intindstat.org/" target="_blank">**Visit association website**</a>


</div>


<!-- 
<div class="col-md-8">
    <h3>cynkra</h3>
    cynkra info
</div>
-->




<div class="col-md-4">
<img style="padding:20px" src="/img/artwork/rconsortium.png" width="200px">
</div>

<div class="col-md-8">
    <h3>R Consortium</h3>
    The central mission of the R Consortium is to work with and provide support to the R Foundation and to the key organizations developing, maintaining, distributing and using R software through the identification, development and implementation of infrastructure projects.

<br><br>
    <a  href="https://r-consortium.org" target="_blank">**Visit consortium website**</a>
</div>






</div>



<div class="row">
<h2 id="silver-sponsors">Silver Sponsors</h2>

<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/openanalytics.png" width="200px">
</div>

<div class="col-md-8">
    <h3>Open Analytics</h3>
Open Analytics is a data science consultancy with a strong involvement in the R Community for over 13 years. We help customers with all data science problems and R is one of our favorite tools in our toolbox. Besides the data science services we also develop 100% open source solutions for any organizational use of R: ShinyProxy (to host Shiny and Dash apps - <a  href="https://shinyproxy.io/" target="_blank">https://shinyproxy.io/</a>), RDepot (to manage your R package repositories - <a  href="https://rdepot.io" target="_blank">https://rdepot.io</a>), R Service Bus (to deploy R-backed data science APIs - <a  href="https://rservicebus.io/" target="_blank">https://rservicebus.io/</a>) and Architect (IDE for all your R development - <a  href="https://getarchitect.io/" target="_blank">https://getarchitect.io/</a>). 

<br><br>
    <a  href="https://www.openanalytics.eu/" target="_blank">**Visit company website**</a>
    
</div>


<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/memverge.png" width="200px">
</div>

<div class="col-md-8">
    <h3>MemVerge</h3>
   MemVerge is pioneering the development of Big Memory Computing that lowers the cost, while increasing the capacity and availability of server memory infrastructure. Memory Machine software from MemVerge virtualizes DRAM and Intel Optane Persistent Memory. Once virtualized, Memory Machine can then provision memory capacity, performance, availability, and mobility to each application.
   
   <br><br>

<a  href="https://www.memverge.com" target="_blank">**Visit company website**</a>
    

</div>






</div>





<div class="row">
<h2 id="bronze-sponsors">Bronze Sponsors</h2>

<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/jumping.svg" width="200px">
</div>

<div class="col-md-8">
    <h3>Jumping Rivers</h3>
Jumping Rivers is a leading provider of bespoke training and consultancy in both R & Python. As an RStudio Full Service Certified Partners, we have a variety of plans for managing RStudio products. From on-demand support to full care plans. If R or Python is crucial to your organisation, we can help.

<br><br>

<a  href="http://jumpingrivers.com/" target="_blank">**Visit company website**</a><br>
<a href= "https://www.jumpingrivers.com/training/all-courses/" target="_blank"> Training </a><br>
<a href= "https://www.jumpingrivers.com/consultancy/managed-rstudio-rsconnect-cloud-production/" target="_blank"> Managed RStudio Services </a>



    

</div>





<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/logo_datahouse.svg" width="200px">
</div>

<div class="col-md-8">
    <h3>Datahouse</h3>
   Founded as a spin-off of the ETH Zurich in 2005, Datahouse has been a big-data specialist ever since the topic first surfaced. We employ a multi­di­sci­plinary team of experts in data analysis, statistics, programming, databases and project management. Datahouse is now a wholly-owned subsidiary of Wüest Partner AG.

<br><br>

<a  href="https://www.datahouse.ch/" target="_blank">**Visit company website**</a>

    


</div>



</div>









<br><br><br>

