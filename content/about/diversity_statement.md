+++
title = "Diversity statement"
description = "What we mean by diversity and how we want to foster it"
keywords = ["Diversity Statement","diversity"]
+++


The aim of UseR! 2021 is to build a high-quality conference in a kind, inclusive, accessible, and safe environment for everyone participating.

As a global event, we wish to honor and celebrate all dimensions of human diversity, across dimensions including, but not limited to: age, body ability, career stage, gender, gender identity, gender expression, geographic origin, language, neurodiversity, political views, race, religion, sexual orientation, and socioeconomic background. <!--This list is not exhaustive and does not pretend to rank dimensions of diversity as more or less important. Some of these dimensions are not going to be explicitly addressed in the context of the event, but-->Respect regarding every aspect of human diversity is expected from the organizers, the presenters, the chairs, and the attendees in all spaces of the conference.

We aim to promote proactively the participation and leadership of people from all backgrounds in the organizing committee, the program committee, the presenters, chairs, and the attendees to the event.
 <br><br><br>

