+++
title = "Meet Our Platinum Sponsors!"
description = "We thank our sponsors for their generous support of the R Community and the useR! conference."
keywords = ["community","sponsors", "useR!","support"]
+++

<img style="padding:20px" src="/img/artwork/roche.svg" width="300px">  



#### Company Profile

With a uniquely broad spectrum of innovative solutions, Roche is a leading healthcare company that has for over 100 years been active in the discovery, development, manufacture, and marketing of healthcare solutions. Our products and services address the prevention, diagnosis, and treatment of diseases, thus enhancing well-being and quality of life.

Headquartered in Basel Switzerland, Roche employs over 85 000 people globally and sells its products in over 150 countries. Our multinational presence reinforces our ability to offer our healthcare solutions worldwide and to anticipate needs in all regions of the world.


<a class="custom-btn" href="https://www.roche.com" target="_blank">Visit company website</a>

<br><br><br>

#### The Lounge Online Booth

Roche. has its own company channel in our Conference Lounge Chat. 
Join the roche channel and talk to people working at Roche.! 


<iframe width="906" height="510" src="https://www.youtube.com/embed/V2mj6dJYG78" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>





<!-- #### Roche Contributions at useR! 2021

**Sponsored Talk** <br>

July 5, 

**Regular Talks** <br>


**Elevator Pitches** <br>

-->


<br><br><br><br>
