+++
title = "Meet Our Platinum Sponsors!"
description = "We thank our sponsors for their generous support of the R Community and the useR! conference."
keywords = ["community","sponsors", "useR!","support"]
+++

<img style="padding:20px" src="/img/artwork/rstudio-flat.svg" width="300px">  



#### Company Profile

RStudio helps people understand and improve the world through data. We build tools that enable robust and reproducible data analysis through programming, paired with tools that make it easy to share insights. Our core software is open source, freely available to anyone. Our professional software equips individuals and teams to develop and share their work at scale.


<a class="custom-btn" href="https://rstudio.com/" target="_blank">**Visit company website**</a>

<br><br><br>

#### The Lounge Online Booth

RStudio has its own company channel in our Conference Lounge Chat. 
Join the RStudio channel and talk to people working at RStudio! 






<!-- #### R Studio Contributions at useR! 2021

**Sponsored Talk** <br>

July 5, 

**Regular Talks** <br>


**Elevator Pitches** <br>

-->


<br><br><br><br>
