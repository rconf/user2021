+++
title = "Meet Our Platinum Sponsors!"
description = "We thank our sponsors for their generous support of the R Community and the useR! conference."
keywords = ["community","sponsors", "useR!","support"]
+++

<img style="padding:20px" src="/img/artwork/appsilon.svg" width="300px">  



#### Company Profile

Appsilon provides innovative data analytics and machine learning solutions for Fortune 500
companies, NGOs, and non-profit organizations. They deliver the world’s most advanced R
Shiny applications, with a unique ability to rapidly develop and scale enterprise Shiny
dashboards. Appsilon has created industry-leading solutions for Johnson & Johnson, Merck,
and many other Global 2000 organizations.

Founded in 2013 by software engineering veterans from Google, Microsoft, Bank of America,
and Domino Data Lab, the company has grown to be a world leader in Shiny and other areas. In
2017, Appsilon became the first organization to successfully scale an R Shiny application
beyond 700 users, and have made many advancements in Shiny since then. They launched
their shiny.fluent and shiny.react open source packages in June 2021, which are a major
breakthrough in Shiny UI.

Appsilon’s mission is to preserve and improve human life through exploration and technology.
Committed to making a positive impact on the world, Appsilon’s team routinely contribute their
time and skills to projects that support positive causes. As part of their AI For Good and Data for
Good Initiatives, they have recently helped to apply computer vision in a conservation project in
Africa and built a tickborne illness tracking app in Shiny for Johns Hopkins University.
Appsilon is a proud RStudio Full Service Certified Partner and frequently collaborates with
RStudio on webinars and workshops, such as this Shiny Masterclass at RStudio Global.

Reach out to us at hello [at] appsilon.com for support with enterprise Shiny applications or
Machine Learning projects. We're always looking for kind, intelligent people to join our team.
Appsilon is a remote-first company with team members in 9+ countries. Go to
appsilon.com/careers to see our current open roles, or email us at join [at] appsilon.com.



<a class="custom-btn" href="https://appsilon.com/" target="_blank">Visit company website</a>

<br><br><br>

 

#### The Lounge Online Booth

Appsilon. has its own company channel in our Conference Lounge Chat. 
Join the Appsilon channel and talk to people working at Appsilon.! 


<iframe width="906" height="510" src="https://www.youtube.com/embed/MYVojGHeKAc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



<!-- #### Appsilon Contributions at useR! 2021

**Sponsored Talk** <br>

July 5, 

**Regular Talks** <br>


**Elevator Pitches** <br>

-->


<br><br><br><br>
