+++
title = "Global Program Committee"
description = "Information about the program committee"
keywords = ["about","program committee","staff","R conference"]
+++

{{< program-committee "https://benubah.github.io/rcentral/useR/pcmap5.html" >}}
_*Some PC Members chose not to associate their names to countries publicly, so they are represented in the map as "PC Member"_

## Program Committee Chairs
{{<table "table table-striped table-bordered" "width:60%">}}
| Name                 | Countries            | Website                                               | Twitter                            |
|:---------------------|:---------------------|:------------------------------------------------------|:-----------------------------------| 
| Dorothea Hug Peter      | Switzerland       | <https://www.wsl.ch/en/employees/hugpeter.html>    | <https://twitter.com/hugdorothea>     |
| Heather Turner          | UK                | <https://www.heatherturner.net>                    | <https://twitter.com/HeathrTurnr>     |
| Janani Ravi             | USA, India        | <https://jravilab.github.io>                       | <https://twitter.com/janani137>       |
| Rocío Joo               | USA, Mexico, Peru | <https://rociojoo.github.io>                       | <https://twitter.com/rocio_joo>       |
| Yanina Bellini Saibene  | Argentina         | <https://yabellini.netlify.app>                    | <https://twitter.com/yabellini>       |
{{</table>}}

## Tutorial Review Committee
{{<table "table table-striped table-bordered" "width:60%">}}
| Name                        | Countries                                 | Website                                            | Twitter                               |
|:----------------------------|:-------------------------------------------|:--------------------------------------------------|:--------------------------------------|
| Andrew Collier              | South Africa         | <https://datawookie.dev>                              | <https://twitter.com/datawookie>   |
| Hannah Frick                | UK, Austria, Germany | <https://www.frick.ws/>                               | <https://twitter.com/hfcfrick>     |
| Malka Gorfine               | Israel               | <https://www.tau.ac.il/~gorfinem/>                    | <https://twitter.com/GorfineMalka> |
| Mine Dogucu                 | USA-Turkey           | <https://mdogucu.ics.uci.edu/>                        | <https://twitter.com/MineDogucu>   |
| Munshi Imran Hossain        | India                | <https://www.linkedin.com/in/imran-hossain-34914821/> |                                  |
| Natalia da Silva            | Uruguay              | <http://natydasilva.com/>                             | <https://twitter.com/pacocuak>     |
| Natalia Soledad Morandeira  | Argentina            | <https://nmorandeira.netlify.app/>  | <https://twitter.com/Nat_Mora_>          |
{{</table>}}

## Abstract Review Committee
{{<table "table table-striped table-bordered" "width:60%">}}
| Name                        | Countries                                 | Website                                            | Twitter                               |
|:----------------------------|:-------------------------------------------|:--------------------------------------------------|:--------------------------------------|
| Ahmadou Dicko               |                                         | <https://ahmadoudicko.com>                         | <https://twitter.com/dickoah>         |
| Alexander Bertram           |                                         |                                                  | <https://twitter.com/bedatadriven>    |
| Andrea Sánchez-Tapia        | Colombia, Brazil                          |                                                  | <https://twitter.com/SanchezTapiaA>   |
| Bahman Rostami Tabar        |                                         | <https://www.bahmanrt.com>                         | <https://twitter.com/Bahman_R_T>      |
| Benjamin Bolker             | Canada                                    |                                                  | <https://twitter.com/bolkerb>         |
| Bernd Bischl                |                                         | <https://www.slds.stat.uni-muenchen.de/>           | <https://twitter.com/BBischl>         |
| Bruno Santos                | Brazil                                    |                                                  | <https://twitter.com/brsantos86>      |
| Christopher Maronga         |                                         |                                                  | <https://twitter.com/krissianyo>      |
| Claudia Vitolo              |                                         |                                                  | <https://twitter.com/Clavitolo>       |
| Colin Fay                   | France                                    | <https://colinfay.me>                              | <https://twitter.com/_colinfay>       |
| Colin Gillespie             |                                         | <https://www.jumpingrivers.com/>                   | <https://twitter.com/csgillespie>     |
| Danielle Navarro            | Australia                                 | <https://djnavarro.net>                            | <https://twitter.com/djnavarro>       |
| Dootika Vats                | India                                     | <https://dvats.github.io/>                         | <https://twitter.com/DootikaV>        |
| Inger Fabris-Rotelli        | South Africa                              |                                                  |                                     |
| Param Jeet                  | India                                     |                                                  |                                     |
| Emi Tanaka                  | Australia                                 | <http://emitanaka.org/>                            | <https://twitter.com/statsgen>        |
| Eun-Kyung Lee               |                                         |                                                  |                                     |
| Federico Marini             | Italy, Germany                            | <https://federicomarini.github.io>                 | <https://twitter.com/FedeBioinfo>     |
| Gabriel Becker              | USA                                       |                                                  | <https://twitter.com/groundwalkergmb> |
| Gergely Daroczi             | Hungary                                   | <https://daroczig.github.io>                       | <https://twitter.com/daroczig>        |
| Germán Federico Rosati      | Argentina, other South American countries | <https://gefero.github.io/>                        | <https://twitter.com/Crst_C>          |
| Ildiko Czeller              | Hungary                                   | <https://ildiczeller.com>                          | <https://twitter.com/czeildi>         |
| Janani Ravi                 | USA, India                                | <https://jravilab.github.io>                       | <https://twitter.com/janani137>       |
| Leonardo Collado Torres     | Mexico                                    | <https://lcolladotor.github.io/>                   | <https://twitter.com/lcolladotor>     |
| Maëlle Salmon               |                                         | <https://masalmon.eu>                              | <https://twitter.com/ma_salmon>       |
| Maia Lesosky                | South Africa                              | <https://www.biostatslab.africa>                   | <https://twitter.com/BiostatsLAB>     |
| Marcela Alfaro Córdoba      | Costa Rica                                | <https://malfaro.netlify.app/>                     | <https://twitter.com/Fichulina>       |
| Maria Tackett               | USA                                       | <https://www.mariatackett.net/>                    | <https://twitter.com/MT_statistics>   |
| Maria Teresa Ortiz Mancera  | Mexico                                    | <https://tereom.netlify.app/>                      | <https://twitter.com/TeresaOM>        |
| Mark van der Loo            |                                         | <https://markvanderloo.eu>                         | <https://twitter.com/markvdloo>       |
| Martin Mächler              |                                         |                                                  | <https://twitter.com/MMaechler>       |
| Michael Dorman              | Israel                                    | <https://geobgu.xyz/>                              | <https://twitter.com/MichaelDorman84> |
| Natalia Soledad Morandeira  | Argentina                                 | <https://nmorandeira.netlify.app/>                 | <https://twitter.com/Nat_Mora_>       |
| Omayma Said                 |                                         | <https://www.onceupondata.com>                     | <https://twitter.com/OmaymaS_>        |
| Paul Murrell                | New Zealand                               | <https://www.stat.auckland.ac.nz/~paul/index.html> |                                     |
| Sevvandi Kandanaarachchi    |                                         | <https://sevvandi.netlify.app/>                    | <https://twitter.com/Sevvandik>       |
| Shelmith Nyagathiri Kariuki | Kenya, other African countries            | <https://shelkariuki.netlify.app/>                 | <https://twitter.com/Shel_Kariuki>    |
| Sina Rüeger                 | Switzerland                               |                                                  | <https://twitter.com/sinarueeger>     |
| Stefanie Butland            |                                         | <https://stefaniebutland.netlify.app/>             | <https://twitter.com/stefaniebutland> |
| Thomas Lin Pedersen         | Denmark                                   | <https://data-imagining.com>                       | <https://twitter.com/thomasp85>       |
| Zhian Namir Kamvar          | USA                                       | <https://zkamvar.netlify.com>                      | <https://twitter.com/zkamvar>         |
{{</table>}}

## Award Review Committee
{{<table "table table-striped table-bordered" "width:60%">}}
| Name                 |
|:---------------------|
|Luis Verde            |
|Natalia Morandeira     |
|Juan Pablo Ruiz Nicolini|
|Stefii Lazerte       |
|Rachel Heyard       |
|Sara Mortara       |
|Teresa Ortiz       |
|Frans van Dunné       |
|Zaynaib Giwa       |
|Matt Bannert       |
|Gabriel Gaona       |
|Marcela Alfaro Córdoba       |
|Vebash Naidoo       |
|Muriel Buri       |
|Elio Campitelli       |
{{</table>}}

<br>
<br>
<br>
