+++
title = "Partners"
description = "Partner Organizations"
keywords = ["community","groups", "R-Ladies", "MiR", "Forwards", "LatinR"]
+++


<div class="row">
<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/rladies.png" width="200px">


</div>
<div class="col-md-8">

<h2 id="R-Ladies"> R-Ladies</h2>

Mission: R-Ladies is a worldwide organization whose mission is to promote Gender Diversity in the R community.
As a diversity initiative, the mission of R-Ladies is to achieve proportionate representation by encouraging, inspiring, and empowering people of genders currently underrepresented in the R community. R-Ladies’ primary focus, therefore, is on supporting minority gender R enthusiasts to achieve their programming potential, by building a collaborative global network of R leaders, mentors, learners, and developers to facilitate individual and collective progress worldwide.

<h4 id="contact-r-ladies"> Contact R-Ladies</h4>
You can find more information on R-Ladies in <a href="https://rladies.org/" target="_blank">our website</a> and follow us on <a href="https://twitter.com/RLadiesGlobal" target="_blank">Twitter</a> .  If you are an R-Lady and want to start a chapter near you, please get in touch via info@rladies.org.


</div>
</div>


<div class="row">
<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/forwards.svg" width="200px">


</div>
<div class="col-md-8">


<h2 id="forwards"> Forwards</h2>

Forwards is a task force set up by the R foundation to improve diversity, equity and inclusion (DEI) in R Foundation related activities (R core, CRAN, useR!, etc). The task force addresses the underrepresentation of women as well as other groups such as LGBTQ, minority ethnic groups, and people with disabilities in the R community.

<h4 id="contact-forwards"> Contact Forwards</h4>
You can find more information on Forwards <a href="https://github.com/forwards/knowledgebase" target="_blank">here </a> and follow us on <a href="https://twitter.com/R_Forwards" target="_blank">Twitter</a>. 



</div>
</div>



<div class="row">
<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/mir.png" width="200px">


</div>
<div class="col-md-8">


<h2 id="mir"> MiR</h2>

Our mission is to provide a space of belonging and support for people who identify as underrepresented minority R useRs. We also provide opportunities for underrepresented minority R useRs to contribute to the R community in various aspects. To learn more about why MiR was started, read <a href="https://medium.com/@doritolay/introducing-mir-a-community-for-underrepresented-users-of-r-7560def7d861" target="_blank">this Medium post.</a>

<h4 id="contact-mir"> Contact MiR</h4>
Follow us on <a href="https://twitter.com/miR_community" target="_blank">Twitter</a>. 
Complete this form to join MiR as a member or an ally:   <a href="https://t.co/89fWVxn8F7?amp=1" target="_blank">MiR Community Registration Form</a>



</div>
</div>


<div class="row">
<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/africaR.png" width="200px">


</div>

<div class="col-md-8">

<h2 id="AfricaR"> AfricaR</h2>

Africa R is a consortium of passionate Africa R user groups and users innovating with R every day, and are inspired to share their experience as well as communicate their findings to a global audience.

This consortium was birthed from the underrepresentation and minority involvement of the African population in every role and area of participation, whether as R developers, conference speakers, educators, users, researchers, leaders and package maintainers.

As a community, our mission is to achieve improved representation by encouraging, inspiring, and empowering the African population of all genders who are underrepresented in the global R community.

The primary objective of Africa R is to support already existing R Users across Africa, and R enthusiasts to embrace the full potential of R programming, through fostering a collaborative continental network of R gurus, mentors, learners, developers and leaders, to help facilitate individual and collective progress worldwide.

<h4 id="contact-africaR"> Contact AfricaR</h4>
You can find more information on AfricaR in <a href="https://africa-r.org/" target="_blank">our website</a> and follow us on <a href="https://twitter.com/AfricaRUsers" target="_blank">Twitter</a> .



</div>
</div>




<div class="row">
<div class="col-md-4">
 
<img style="padding:20px" src="/img/artwork/latinr.png" width="200px">


</div>

<div class="col-md-8">

<h2 id="latinR"> LatinR</h2>

In October 2017, the R Foundation Conference Committee announced on the R User Group Organizers (RUG) Slack space that the R Foundation was interested in hosting R events with an academic focus in regions that were not covered by useR!. In less than a week a group of R-Ladies had our first video conference organized to think about how to face the challenge. This quick response was not just luck, but the consequence of a year in which the R community - through R-Ladies chapters, local RUGs and social media presence - grew significantly in South America. By mid-November everything was defined: a name, a place, a date and a motivated international organizing committee. The first edition was held in Buenos Aires, Argentina, the second in Santiago de Chile, Chile and the third was virtual due to COVID-19. We like to say: we are more than a conference, we are a community, a Latin America R Community. 
<h4 id="contact-latinR"> Contact LatinR</h4>
You can find more information on LatinR in <a href="https://latin-r.com/" target="_blank">our website</a> and follow us on <a href="https://twitter.com/LatinR_Conf" target="_blank">Twitter</a> .



</div>
</div>









<br><br><br>
