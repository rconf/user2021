+++
title = "Welcome to useR! 2021"
description = "Join useR! 2021. useR! is the annual conference of the R Language for Statisical Computing. Meet R enthusiasts from more than 100 countries from Buenos Aires to Zurich, Nairobi and Sydney. "
keywords = ["Rstats","programming","conference","week of R"]
+++


## It's on. 5 Days of R! 

  <!-- <ul class="countdown"></ul>
            <li class="cdelement firstel" style="display: inline-block; font-size: 1em;
        list-style-type: none;"><span id="days" style="display:block;font-size: 2.3em;font-weight:bold;"></span>DAYS</li>
            <li class="cdelement" style="display: inline-block; font-size: 1em;
        list-style-type: none; "><span id="hours" style="display:block;font-size: 2.3em;font-weight:bold;"></span>HOURS</li>
            <li class="cdelement" style="display: inline-block; font-size: 1em;
        list-style-type: none;"><span id="minutes" style="display:block;font-size: 2.3em;font-weight:bold;"></span>MINUTES</li>
            <li class="cdelement" style="display: inline-block; font-size: 1em;
        list-style-type: none;"><span id="seconds" style="display:block;font-size: 2.3em;font-weight:bold;"></span>SECONDS</li>
          </ul>-->
<br>
<br>
<a class="custom-btn" href="https://www.conftool.org/user2021/sessions.php" target="_blank">Enter Your Session Through Conftool</a>
<br><br><br>

**Note**: You have to be logged in to conftool in order to see the links to sessions. Please do not share the sessions links with unregistered people. 
## Where is What ? 

Participation starts in Chat. This is where everyone from everywhere meets.
From Buenos Aires to Zurich. From Nairobi to Sydney. From Keynote Speaker to helping hand. 
From core developer to casual useR. Our Chat is a lightweight, mobile & screen reader friendly chat platform to make sure the hurdle to take part and interact is as low as possible for everyone. Talk to the R community, start calls, join tutorials or talks or reach out for help. The chat is where we meet during this week of R!

- We meet and gather in our chat. This is also where you find the links to all webinars and streams
- Keynotes will be streamed on YouTube and also have corresponding chat channels

- **Regular talks** will take place in **zoom** sessions, but als be streamed to YouTube, again they have **chat channels**
- **Elevator pitches and panels** will take place in **zoom webinars**
- **Panels will take place in zoom webinars**
- **Tutorials** will take place on **zoom**, participation and join links will be shared with registered participants. 
- **Incubators** will take place in **zoom sessions /w breakout rooms**.
- We offer an infrastructure to start **jitsi calls for smaller groups**, e.g., during breaks (simply run /jitsi Anna Nick to start a video call with Nick and Anna)
 
**Please make sure you do not leak any non-public links to unregistered participants** 


## Important useR! 2021 Guidelines

[Code of Conduct](/participation/coc/)

## 5 Days of More Than R - Our Social Events

Our social programs starts before useR! 2021 even begins!
Make sure to say *hi* at one of our *Welcome and Navigation Guide* sessions 
or share your thoughts on useR! in our *My First useR!* session. Our social program includes yoga classes, cartoon art tutorials, community events, a trivia and a movie screening. Learn more on our [social events page](/program/social)! useR! is looking forward to hang out with you! 


<img src="/img/artwork/marmot-yoga.png" width="150">
<img src="/img/artwork/marmot-popcorn.png" width="150">
<img src="/img/artwork/marmot-mir.png" width="120">
<img src="/img/artwork/marmot-artist.png" width="150">




## What's a Marmot? 

A marmot is whistling gnawer living in mountain regions mostly. [In a nod to Switzerland and the city of Zurich maRmots became the mascot of useR! 2021](/blog/2020/11/26/welcome-margot-the-marmot/). During useR! 2021, maRmots will scurry around the conference, mostly in our Chat always on the look out to help you. If you have questions, can't find a virtual room or channel, have a technical problem or are lost otherwise, contact one of your maRmots sending them a direct message (DM). 


## Thanks to ... 

We would like to say thank you to all of our sponsors, partners and all the volunteers that made the global virtual adventure that useR! 2021 is possible. What a team effort !


<br><br>

<script>

//countdown timer
(function () {
const second = 1000,
  minute = second * 60,
  hour = minute * 60,
  day = hour * 24;

// Using UTC Time here, remember JS is off by 1 month cause
// counting months starts at zero! this is actually Jul 4, 2021 22:15:00
let countDown = new Date(Date.UTC(2021,6,4,22,15,0)).getTime(),
x = setInterval(function() {    

  let now = new Date().getTime(),
      distance = countDown - now;

  document.getElementById("days").innerText = Math.floor(distance / (day)),
    document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
    document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
    document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

  //do something later when date is reached
  if (distance < 0) {
     headline = document.getElementById("headline");
document.getElementById("days").innerText = 0;
document.getElementById("hours").innerText = 0;
document.getElementById("minutes").innerText = 0;
document.getElementById("seconds").innerText = 0;
     //countdown = document.getElementById("countdown");
     //content = document.getElementById("content");

    headline.innerText = "Welcome to useR! 2021";

    //countdown.style.display = "none";
    //content.style.display = "block";

    clearInterval(x);
  }
  //seconds
}, 0)
}());
</script>
