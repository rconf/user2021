+++
title = "Keep online option at conferences: it makes them more inclusive"
date = "2021-10-14"
tags = ["hybrid conferences","inclusion", "accessibility"]
author = "The UseR! 2021 Team"
categories = ["useR!"]
banner = "/img/artwork/marmot-artist.png"

+++

We worked hard to make an inclusive useR! 2021 for our diverse community and the virtual setting of the conference was fundamental in that purpose. We are proud of what we were able to achieve and happy about the lessons learned along the way.

As the [organizing team](https://user2021.r-project.org/about/global-team/), we wrote a correspondence advocating for the continuation of online conferences as means to remove barriers for inclusion and it was recently published in [Nature](https://www.nature.com/articles/d41586-021-02752-8). You can also read the open access accepted version [here](https://rociojoo.netlify.app/blog/2021-10-13-nature-corr/).

To cite our publication:

Joo, R. (2021). Keep online option at conferences—It makes them more inclusive. Nature, 598(7880), 257–257. https://doi.org/10.1038/d41586-021-02752-8

```
@article{joo_keep_2021,
  title = {Keep Online Option at Conferences \textemdash{} It Makes Them More Inclusive},
  author = {Joo, Roc{\'i}o},
  year = {2021},
  month = oct,
  journal = {Nature},
  volume = {598},
  number = {7880},
  pages = {257--257},
  publisher = {{Nature Publishing Group}},
  doi = {10.1038/d41586-021-02752-8},
  copyright = {2021 Nature}
}
```

<p align="center">
<img src="/img/artwork/usermap.svg"
     alt="A worldmap with the number of participants to useR! 2021 for each country. We reached 122 countries"
      />
</p>
<br>
<br>
<br>
<br>
