+++
title = "How to improve conference accessibility for screen-reader users—an interview with Liz Hare"
date = "2021-11-04"
tags = ["accessibility", "inclusion"]
author = "Rocío Joo, Silvia Canelón, Joselyn Chávez, Andrea Sánchez-Tapia"
categories = ["useR!"]
banner = "/img/artwork/marmot-user.png"
+++

Do you remember meeting any people with sensory disabilities (e.g., blindness, deafness) at conferences? Have you ever wondered if online conference platforms are accessible for everyone?

We talked to [Liz Hare](https://twitter.com/DogGeneticsLLC), a statistical geneticist who participated in the [useR!&nbsp;2021](https://user2021.r-project.org/) organizing team, about conference accessibility. Liz is blind, uses screen reading software, and contributed significantly to the accessibility practices for the conference. We covered her experience with other conferences and their platforms, her accessibility work at useR!—the good and the bad—, and what makes a presentation accessible. The following are extracts of the interview with Liz. Some of the questions were reworded for the purpose of readability of  this post.


{{< youtube B_ZmcYoDmhU >}}

<a href="https://youtu.be/B_ZmcYoDmhU"> Check the full interview on YouTube</a>

## Previous conferences

*Q: Could you tell us about your experience with trying to attend conferences?*

Conferences vary because several software components go into them. Earlier this year, I gave a talk with [Silvia Canelón](https://silvia.rbind.io) about the use of alt text in R TidyTuesday submissions. We had already submitted our abstract and everything, and then we found out that the conference was going to be hosted on Crowdcast and I couldn't access that at all. So they had to patch me in via Zoom and I wasn't able to attend the rest of the talks in real time with everybody else.

R Medicine is using Crowdcast also, so that one won't be accessible. I have several conferences in my field that I've had to advocate really hard for access to, and ones I'm waiting to hear from that are really resisting telling me what platform they'll be on, so... it's a really mixed bag.

[...] Most conferences have some sort of boilerplate standard legal disclaimer, if they have anything at all. You often have to get in touch with them to ask. In the last year and a half I've had to say, "how are you hosting this conference?" If it's going to be on GatherTown or, there's another really visual one [Spatial chat], that is just completely inaccessible. And it has neat features, where you can see a map of where the people are, and you can see your friends and go up to them and start a conversation, and that's great, but not everyone can access it.

## Presentations

*Q: What makes presentations accessible or inaccessible?*

**Two channels: the speaker and the slides.** One of the assumptions at a lot of conferences is that the audience will be receiving information on two channels: they'll be hearing the speaker talk about their slides, and they'll be seeing the details that are in the slides. When you have a technical conference like useR!, where you're actually showing how to write code and things like that, and you're missing one of those channels [because of sensory disabilities or other visual or auditory processing disabilities], it can be a bummer.

**Share presentations in advance.** One way to make your presentation accessible is to make it available in advance so that I can read along with you on my device. My screen reader will not read slides that are shown in a video on zoom. So, having advanced access is really helpful to me, using language that helps me tie what I'm reading in with what you're saying, and trying to avoid pointing at things and saying, "as you can see from this" or "this thing over here does this." So, using specific language so that everyone knows what you're talking about.

**Navigating through the slides** First, I checked that I could navigate easily through the text. And that the text was laid out and used aspects of—in this case, they were all RMarkdown-created slides so—they used aspects of HTML, that made it really easy for me not just to hear the words but to get the context. So, for example, a nested list contains interesting relationships between items and sub-items. Whereas if you save that information into a PDF, all that would get lost from the screen reader perspective. So it was really great to just be able to navigate through them and understand the structure and how the different information presented in the slides related to each other, and the tables were nicely formatted and easy to navigate.

*Q: Do you have any advice for people who have never thought about making an accessible presentation but would like to start?*

This committee has done a terrific job of putting together [accessibility guidelines on the website](https://user2021.r-project.org/participation/accessibility/) for useR!&nbsp;2021. It's a great place to start because I've had [access to] a lot of links that explain things like how to measure the contrast and making sure that you have enough contrast in your presentation, how to make sure that you are not using colors that can be easily confused by people with color blindness, and how to write good alt text. There are all kinds of resources there. You should look to consult with a person who actually has the experience using those accessibility features. Because there's really no substitute for someone who is very familiar with using a screen reader and navigating, for example, websites or presentations. There's really no substitute for the knowledge of people who are solving these problems for themselves every day.


## Alt text

*Q: What is good alt text?*

Alt text is a text description of a data visualization or anything else on the web. As I mentioned earlier, Silvia Canelón and I did some work on looking at the alt texts that were or were not included in TidyTuesday submissions. I was able to break down some of the ingredients you needed to have a good alt text. It's really good to give the user an orientation to what type of plot or graph it is; is it a line graph, is it a bar chart? Sometimes, some of these fancy or new kinds of tidyverse charts need a little bit more orientation and description. It's also good if you tell us what's on the X axis, what's on the Y axis, what scale the measurements are on, what kind of numbers are involved, and lastly—and most importantly—you really need to tell us what the data is saying and why you included it. So, that information is usually not available in the title or caption of a data visualization, say, in an academic journal. But it's really critical for alt text, because the person isn't receiving the information visually, to tell us how the data are related to each other and what you want people to see in your data visualization. More information on this work is available on the [GitHub repo](https://github.com/spcanelon/csvConf2021).


## useR!&nbsp;2021

*Q: When to introduce accessibility components into a conference?*

In a conference like useR!&nbsp;2021, accessibility has to be baked into the whole thing from the start. It's also a lot easier and cheaper and less effort to do it if you build it in from the start. [...] And a lot of times designing things to be accessible improves the overall design of them. The other day I heard an explanation: "if it's too hard to write an alt text for your data visualization then there's probably too much going on in your data visualization." Same with a website. If you keep it accessible, it will be streamlined for everybody.

*Q: What did you work on at useR!&nbsp;2021 accessibility-wise?*

I worked with the teams who were doing communication about the conference—emails, tweets, things like that—to make sure they had alt text descriptions for all the cute [maRmots](https://user2021.r-project.org/blog/2020/11/26/welcome-margot-the-marmot/) and other things. I worked to make sure that the website was accessible, that the registration process was accessible, that the schedule could be read and understood and navigated with a screen reader.

*Q: What was accessible (or inaccessible) at useR!&nbsp;2021?*

One of the greatest and easiest things was that Zoom is very accessible. I was able to navigate all the Zoom things I needed to navigate, both as a participant and as chair of a session. Probably the most challenging part to navigate was that we ended up using Slack as the glue to have discussions, to have announcements about which sessions were in which Zoom rooms. And, unfortunately, with a screen reader you can't reliably follow links in Slack, so it took a lot of work to get access to the database of talks, where they would be hosted, and where the slides were.

*Q: How to reach out to the part of the community that has been excluded by inaccessible practices in the past to participate in the conference?*

I think we did a really good job by having accessibility information on the website for the conference. There was, under participation, there was a whole section for accessibility. [...] The fact that I can find the information for myself, that I don't have to request it from anyone, it's really welcoming if you see that it has been considered in advance. [...] If I go on a website, and I see that somebody understands that basic [accessibility] stuff, and that the slides will be available for me to listen along to while the person talks so that I can have both channels of information, and things like that, it's really great. And having a contact [email], which we had all along, we had, you know, people could email their questions about accessibility. Having accessibility information available in multiple ways. For example, for people with different sensory disabilities, you wouldn't want to have a video that wasn't captioned explaining to people what accessibility steps you were taking. You'd want to have it both in writing and in video.

It is much more welcoming when the guidelines are already in place. I think it was a huge achievement that we asked presenters to agree to follow the accessibility guidelines. They didn't all, but it really signaled to the community that we were going to take this seriously.

----

We would like to thank Liz for sharing her experiences and knowledge. We hope you will find this information useful, whether you're organizing a conference or looking to make your presentations, website, or social media more accessible.

For conference organizers, the main take-home messages are:

+ Include people with disabilities in the organizing team. Their experience is essential and will make work go smoother. 
+ Think about accessibility from the start. Some decisions will be hard to correct later during the event organization
+ Communicate your accessibility practices to the general public to make your event welcoming to disabled people and foster accessibility practices among non-disabled people. In the end, accessibility practices are beneficial to everyone. 

We call digital conference and chat plaform developers to improve the accessibility of their products. With the transition towards online meetings and remote work, the demand for these products increased. Still, few of them are fully compatible with screen-readers. Liz mentioned some in the interview, but we also tested others, so this is not an exhaustive list. The lack of screen reader usability makes blind people's remote work, collaborations, and conference participation difficult. Going online is an excellent opportunity to revert this.

<br>
<br>
<br>
<br>
