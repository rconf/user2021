+++
title = "Prizes awarded during useR! 2021"
date = "2021-11-18"
tags = ["awards","inclusion", "accessibility"]
author = "The UseR! 2021 Team"
categories = ["useR!"]
banner = "/img/artwork/marmot-artist.png"

+++

This year, there were awards for Early Career Researchers (students and postdocs), accessible presentations, and a Spotlight recognition to people behind the wheels of the R community.

The Early Career Researchers awards had reviewers that evaluated presenters' work and, for accessibility, the useR! 2021 Diversity Team reviewed all the presentations. The spotlight mentions were decided by popular vote during the conference.

### Early Career Awards

* __Outstanding Technical Note:__ Balint Tamasi. [tramME: Transformation Models with Mixed Effects](https://user2021.r-project.org/participation/technical_notes/t146/technote/)

* __Outstanding Lightning Talk:__	Batool Almazrouq. [Make Your Computational Analysis Citable](https://youtu.be/IrV2l3qdon8)

* __Outstanding Lightning Talk:__	Varsha Ujjini Vijay Kumar, Dilinie Seimon. [rspatialdata: a collection of data sources and tutorials on downloading and visualising spatial data using R](https://youtu.be/nLikORFBKgk)

* __Outstanding Regular Talk:__	Lucas van der Meer. Tidy Geospatial Networks in R. [Session 3B](https://youtu.be/m_ORcYtoEDk)

* __Outstanding Regular Talk:__	Liam D. Bailey. Using R6 object-oriented programming to build agent-based models. [Session 7A](https://youtu.be/Ey2bG_diVoA?t=2909)

### Accessibility Mention

* Natalia Morandera - [Obtaining reproducible reports on satellite hotspot data during a wildfire disaster](https://nmorandeira.github.io/Fires_ParanaRiverDelta/)

* Virginia A. García Alonso; Paola Corrales; Claudia A. Huaylla; Andrea Gómez Vargas; Joselyn Chávez; Denisse Fierro Arcos - [Using R in Latin America: the great, the good, the bad, and the ugly](https://encuesta-user2021-en.netlify.app/)

* Lluís Revilla Sancho - [Packages submission and reviews; how does it work?](https://user2021.llrs.dev/)

### Spotlight mentions

* Heather Turner
* Paola Corrales



<br>
<br>
Sponsored by
<br>
<br>

<p align="center">
<img src="/img/sponsors/CRClogo.jpg"
     alt="CRC Press - Taylor and Francis Group - Chapman & Hall" />
</p>
<br>
<br>
