+++
title = "The role of the R community in the Research Software Engineering (RSE) movement"
date = "2021-09-04"
tags = ["Research Software Engineering","RSE"]
author = "Mario Antonioletti, Matt Bannert, Emma Rand, Heidi Seibold, Nicholas Tierney, Heather Turner"
categories = ["RSE","Community"]
banner = "/img/artwork/rse-logo.png"

+++

What does a Research Software Engineer (RSE) who uses R do? How can we promote RSE career paths? How can we identify and highlight which funders are most research software friendly? How can we increase visibility of RSEs in the R community? Could we develop a podcast on ‘meet the R-engineers’ or collaborate with an existing R podcast? What should a book “Research Software Engineering with R” contain?  How do we develop an RSE for R users community?

Inspired by Heidi Seibold's useR! 2021 keynote [*Research software engineers and academia*](https://www.youtube.com/watch?v=dag9l0GFci8&list=PL4IzsxWztPdmHoJwIVa4um44w2GMjctmP&index=5) these were the questions we worked on in an incubator session during the conference. 

Heidi Seibold, Matt Bannert and Heather Turner chaired the session, which was attended by around 30 participants. We were joined by guests Ania Brown (RSE at the University of Southampton, UK and Trustee at the Society of Research Software Engineering) and Mario Antonioletti (RSE at Software Sustainability Institute, UK).

The session began with a short [talk on the RSE movement](https://www.youtube.com/watch?v=Xhwh80aTF0k) from Ania Brown.
We then directed people to move into breakout rooms titled by topics that had been proposed by a wide team of session facilitators and advisors working in RSE&#8209;related roles. In the end, we had small groups discussing seven of the proposed topics. We have pulled together the summaries from these breakout groups below.

<a href="https://www.youtube.com/watch?v=Xhwh80aTF0k"><img src="/img/artwork/rse-intro.jpg" width=550>Ania Brown's talk "Society of Research Software Engineering" on YouTube.</a>

## What does an RSE who uses R do?

Anna Krystalli and Nick Tierney shared similar paths to becoming an RSE: starting out in research, really enjoying the software aspect of their work, and gradually becoming more focused on the software aspects of the research process. A common theme was the need for empathy in the design of software and documentation - wanting to make software that enabled researchers to do their job more efficiently, rather than spend time learning the nuances and minutiae of software to complete their work. Common daily tasks included: cleaning up data, automating repetitive tasks, writing R packages for analyses, developing documentation for existing projects, and encouraging new users.

Others in the group shared what they do in their role, which wasn’t always officially “research software engineer”. This included: creating analysis templates (e.g., this package of [R markdown styles and templates for official reports](https://github.com/inbo/inbomd)); building tools or R packages to perform statistical analyses; developing checks for code quality (see the [checklist](https://inbo.github.io/checklist/) package), and storing or sharing data in an open format under version control (see the [git2rdata](https://ropensci.github.io/git2rdata/) package).

Finally, we discussed how to get started as an RSE. Some ideas we came up with were to start teaching software to your research group, or to find common tasks that might be repetitive or annoying for other researchers and to explore ways to automate or simplify these processes. As for the skills needed to get started as an RSE, we believe if you like research, but are also interested in how software is used by other researchers, that should form a great base to start on the path to becoming an RSE. You do not need to have a background in software engineering, although it certainly wouldn’t go astray! We believe the more important side of an RSE is knowing the context of the areas of research and how it is conducted. It is this blend of having research experience, interest in software, and desire to improve research experience, that helps form a base for being an RSE.

## How can we promote RSE career paths?

We had a lot of discussion about the fact that research software does not “count” as achievement in academic contexts (industry may vary?), but only paper publications do. What can help to change this?

- Teach / learn about research software and its importance.
- Make software a first class citizen in science (count it as a valuable research output).
- Value maintenance of software

We also talked about what could help RSEs in their career path.

- Allow for less stiff career paths in science, e.g. introduce RSE career path in addition to careers as researcher, librarian, etc.
- Offer permanent/prestigious positions and group leader positions.
- Make term RSE more widely known.

We found it important to invoke change both top down and bottom up.

**Bottom up**: Build grass-roots communities.  
**Top down**: Institutions, funders, and journals have the power to make change.

## How can we identify and highlight which funders are most research software friendly? 

The scope of the topic was widened somewhat to finding funding (or effort) to help maintain a successful existing project. Although most funders are happy to provide seed capital to create or support an existing piece of software it is best to diversify revenue streams to help support a piece of software and not to rely on an existing single source of funding. With this in view:

- It is important to build networks to become aware of opportunities and how to exploit them. The RSE movement is such a network to join.
- Use connections with Master/PhD programmes to help develop focused functionality. If your project is open source you can try the [Google Summer of Code](https://github.com/rstats-gsoc/) to sponsor a student.
- Use a donation button (but it provides no continuity in the funding stream).
- Consider a dual licensing scheme for commercial/academic use.

## How can we increase visibility of RSEs in the R community?

Many people in the R community do not know what an RSE is or identify as an RSE when they could! Our discussion had two main threads:

1. Promoting RSE as a discipline/career

 - Share relevant talks on social media, e.g., [Mike Crouchers keynote at JuliaCon 2018](https://youtu.be/8ZSaAM8hhJ4).
 - Invite RSE speakers (especially R users) to R events.
 - Share RSE event announcements/listings.
 - Participate in mentoring (the RSE Society is announcing a mentoring scheme at [SeptembRSE](https://septembrse.github.io/#/event/T1026)).
 - Share RSE jobs (the RSE Society has a [vacancies](https://society-rse.org/careers/vacancies/) page).
 - Apply for funding to support RSE work/outreach (some potential funders: Society of RSEs, CZI, Code for Science and Society, Wellcome Trust, Gordon and Betty Moore Foundation.)


2. Identifying as an RSE/building community

 - Join your local [RSE Association](https://researchsoftware.org/assoc.html) (the UK-based Society of Research Software Engineering is open to international members and has a [LinkedIn group](https://www.linkedin.com/groups/13564426/)) and/or the [RSE Slack](https://society-rse.org/join-us/#slack)
 - Create RSE channels on R Slacks, e.g. [R-Ladies community Slack](https://rladies-community-slack.herokuapp.com/).
 - Use the #RSEng hashtag on Twitter (with #RStats, #RStatsES etc).
 - Write RSE-related posts on R blogs.
 - Create a newsletter for R + RSE community.
 - Start/contribute to a podcast.


## Could we develop a podcast on ‘meet the R-engineers’ or collaborate with an existing R podcast?

The RSE Stories [podcast/blog](https://us-rse.org/rse-stories/) is more-or-less exactly in line with the initial idea of giving people who work in RSE an opportunity to share their story. Though there were conflicting views within the incubator group w.r.t. the preferred focus of a podcast (mostly stories vs. tutorial/applied content), the group agreed that new things call for new people. Many of the incubators' participants were already involved in community and/or development work, so it would be practical to collaborate with an existing effort and/or act as co-hosts. There is possibly a chance to cooperate with Vanessa Sochat of the U.S.-based RSE Stories podcast. A collaboration or European / African version could be a reasonable starting point. 

The following existing effort were mentioned by participants:
- [Bioconductor Developer's Forum](https://www.youtube.com/playlist?list=PLdl4u5ZRDMQQLMupAtEzm2y4gUIUm_1n6) (Monthly teleconference with RSE content)
- [Why R? Webinars](https://www.youtube.com/watch?v=3RkBEva4xRk&list=PLKMUlj_pGn_ldSI9_xIae6tOozrMNYa96)
- [RSE Stories podcast](https://us-rse.org/rse-stories/posts/)


## What should a book “Research Software Engineering with R” contain?

We identified several different approaches and topics which could form the basis of multiple books or major sections within one book. Three themes emerged:

1. Materials covering the fundamental practical and technical elements that would allow R users to become software engineers using R. This could be similar to [Research Software Engineering with Python](https://merely-useful.tech/py-rse/) by Damien Irving, Kate Hertweck, Luke Johnston, Joel Ostblom, Charlotte Wickham, and Greg Wilson, which itself draws on many workshops by [The Carpentries](https://carpentries.org/). It might cover universal tools such as the Unix shell, git, and pipeline toolkits (including R specific ones like targets) and package development, testing and deployment. In addition, it could cover thinking like a research software engineer and how to be a good contributor.

2. Materials explaining the benefits of R-focused RSE to R users and to existing RSE teams. The [The Turing Way](https://the-turing-way.netlify.app/welcome.html) handbook to reproducible, ethical and collaborative data science covers much of this very well and it might be sensible and productive to contribute the R-specific perspectives there.

3. More advanced topics likely to be useful for R-using RSEs, such as managing memory, big data, cloud and parallel computing, database connectivity and data products such as Shiny dashboards.

## How do we develop an RSE for R users community?

The group discussion covered several ideas that were considered in other breakouts, emphasizing the relevance of these ideas, while providing a plan of action to develop an RSE for R users community

- Find interested people and set up a core team (probably the most difficult issue).
- Make the RSE concept explainable to interested people, e.g., by writing a manifest.
- Identify crosscutting topics in the RSE and R communities (e.g. Open Science/Data).
- Invite R users to RSE events and vice versa.
- Start with user stories as in the RSE Stories podcast.

## Summary

The incubator was an enjoyable and productive session that in itself helped to raise awareness of the RSE movement in the R community. We hope that participants and other interested people in the R community will follow suggestions put forward and build on these ideas.

For R users that want to get more involved in the RSE movement we can suggest three immediate actions:

 - Join the #r-users channel on the [RSE Slack](https://society-rse.org/join-us/#slack)
 - Join your local [RSE Association](https://researchsoftware.org/assoc.html)
 - Participate in [SeptembRSE](https://septembrse.society-rse.org/), an online RSE conference running throughout September.

We thank the great team that helped to prepare and run this session:

 - Chairs: [Matt Bannert](https://twitter.com/whatsgoodio), [Heidi Seibold](https://twitter.com/HeidiBaya), [Heather Turner](https://twitter.com/HeathrTurnr)
 - Guests: [Mario Antonioletti](https://www.software.ac.uk/about/staff/person/mario-antonioletti), [Ania Brown](https://society-rse.org/about/governance/anna-ania-brown/)
 - Breakout Facilitators: [Jan Dietrich](https://twitter.com/tscheypidi), [Anna Krystalli](https://twitter.com/annakrystalli), [Emma Rand](https://twitter.com/er13_r), [Jochen Schirrwagengot](https://www.linkedin.com/in/jochen-schirrwagen-280799106/), [Nicholas Tierney](https://twitter.com/nj_tierney)
 - Advisors: [Stefanie Butland](https://twitter.com/StefanieButland), [Olina Ngwenya](http://www.cidri-africa.uct.ac.za/cidri-africa/about/staff), [Karthik Ram](https://twitter.com/_inundata), [Anelda Van der Walt](https://twitter.com/aneldavdw)
 - Zoom Host: [Faith Musili](https://twitter.com/musili_faith)
 
Thanks also to the participants, whose notes were used to prepare this post. 
<br>
<br>
<br>
<br>
