+++
title = "Transcript for Liz Hare's interview"
description = "We talked with Liz Hare about conference accessibility"
keywords = ["Blog","Accessibility"]
+++

Thank you for coming Liz. Can you tell us
something about yourself?

Hi, yeah I have been using R for over 15 years.
I am a statistical geneticist and I do a lot

of other statistical work focusing on working
dogs and their behavior and health. I'm blind

and I use screen reading software. I got interested
in accessibility at first because after the

conference two years ago, there was a bit
of a brag on Twitter about the conference

and how inclusive it was, and I kind of pointed
out that it wouldn't really be that inclusive

for blind people. And they invited me to join
Forwards, and so I have been working on conference

accessibility since then.

What kind of accessibility practices
make it possible for you to participate in

a conference in general?

A conference like useR! 2021, really accessibility,
has to be kind of baked into the whole thing

from the start. It's also a lot easier and
cheaper and less effort to do it if you build

it in from the start. My personal needs are
screen reader accessibility.

I worked with the teams who were doing communication
about the conference - emails, tweets, things

like that - to make sure they had alt text
descriptions for all the cute maRmots, and

other things like that. I worked to make sure
that the website was accessible, that the

registration process was accessible, that
the schedule was something that could be read

and understood and navigated with a screen
reader. What we're going to focus on today

is what presenters do to make their talks
accessible.

For many people with sensory disabilities
or even other visual or auditory processing

disabilities, one of the assumptions at a
lot of conferences is that the audience will

be receiving information on two channels;
they'll be hearing the speaker talk about

their slides, and they'll be seeing the details
that are in the slides. When you have a technical

conference like R, where you're actually showing
how to write code and things like that and

you're missing one of those channels, it can
kind of be a bummer.

One way to make your presentation accessible
is to make it available in advance, so that

I can read along with you on my device. My
screen reader will not read slides that are

shown in a video on Zoom. So, having advanced
access is really helpful to me, using language

that helps me tie what I'm reading in with
what you're saying, and trying to avoid kind

of pointing at things and saying, "as you can
see from this" or" this thing over here does

this". So, using specific language so that
everyone knows what you're talking about.

Great, thank you Liz. So, as a screen reader
user what kinds of things did you check for

when evaluating the speaker slides for accessibility?

That's a great question. First I checked that
I could navigate easily through

the text. And that the text was laid out
and used aspects of, in this case they were

all RMarkdown-created slides so they used
aspects of HTML, that made it really easy

for me not just to hear the words but to get
the context. So, for example, a nested list

contains interesting relationships between
items and sub items. Whereas if you save that

information into a PDF all that would get
lost from the screen reader perspective. So

it was really great to just be able to navigate
through them and understand the structure

and how the different information presented
in the slides related to each other, and the

tables were nicely formatted and easy to navigate.

Another really important component for me
as a screen reader user is alt text. Alt text

is a text description of a data visualization
or anything else on the web. As I mentioned

earlier, Silvia Canelón and I did some work
on looking at the alt texts that were or were

not included in TidyTuesday submissions. I
was able to kind of break down some of the

ingredients you needed to have a good alt
text. It's really good to give the user an

orientation to what type of plot or graph
it is; is it a line graph, is it a bar chart?

Sometimes, some of these fancy or new kinds
of tidyverse charts need a little bit more

orientation and description. It's also good
if you tell us what's on the X axis, what's

on the Y axis, what scale the measurements
are on, what kind of numbers are involved,

and lastly - and most importantly - you really
need to tell us what the data is saying and

why you included it. So, that information
is usually not available in the title or caption

of a data visualizations, say, in an academic
journal. But it's really critical for alt

text, because the person isn't receiving the
information visually, to tell us how the data

are related to each other and what it is that
you want people to see in your data visualization.

Compared with other conferences, what are
the main accessibility challenges that you

encountered before, and did useR! improve in
some aspects?

One of the greatest and easiest things was
that Zoom is very accessible. I was able to

navigate all the Zoom things I needed to navigate,
both as a participant and as chair of a session.

Probably the most challenging part to navigate
was that we ended up using Slack as the glue

to have discussions, to have announcements
about which sessions were in which Zoom rooms.

And, unfortunately, with a screen reader you
can't reliably follow links in Slack, so it

took a lot of work by this Committee on the
back end to get me access to the database

of talks and where they would be hosted and
where the slides were.

Other conferences kind of vary because there's
usual several software components that go

into it. Earlier this year I gave a talk with
Silvia Canelón about the use of alt text

in R TidyTuesday submissions. And we had already
submitted our abstract and everything, and

then we found out that the conference was
going to be hosted on Crowdcast and I couldn't

access that at all. So they had to kind of
patch me in via Zoom and I wasn't able to

attend the rest of the talks in real time
with everybody else.

R Medicine is using Crowdcast also, so that
one won't be accessible. I have several

conferences in my field that I've had to advocate
really hard for access to, and ones I'm waiting

to hear from that are really resisting telling
me what platform they'll be on, so... it's

a real mixed bag.

Because we are not used to thinking about
how to include everybody, most conferences

are not inclusive, right? And people that
need to be included, I guess that they can

assume, fairly, that they're not going to
be included on average.

Exactly.

So we actually need to not only build the
tools, and give them material, and think about

organizing the conference in ways that they
are included, but we also need to communicate

that message to the whole community, to the
broader community. How can we improve in that

sense?

I think we did a really good job by having
accessibility information on the website for

the conference. There was, under participation,
there was a whole section for accessibility.

Most conferences have some sort of boilerplate
standard legal disclaimer or something, if

they have anything at all. You often have
to get in touch with them to ask. I mean in

the last year and a half I've had to say,
"how are you hosting this conference?" If

it's going to be on GatherTown or, there's
another really visual one, that is just completely

inaccessible. And it has neat features, where
you can see a map of where the people are,

and you can see your friends and go up to
them and start a conversation, and that's

great, but not everyone can access it.

Spatial Chat.

Yes! Yes, you got it. So, the fact that I
can find the information for myself, that

I don't have to request it from anyone, it's
really welcoming if you see that it has been

considered in advance. I, a couple months
ago, had a call for papers for a conference

in my field. There was no accessibility information
on the website. I got in touch with them to

ask what platform they had chosen, and luckily
they hadn't chosen one yet. Hopefully that's

going to work out.

People don't realize that when you screen
share on Zoom that the screen reader can't

read that kind of video content. If I go on
a website, and I see that somebody kind of

understands that basic stuff, and that the
slides will be available for me to listen

along to while the person talks so that I
can have both channels of information, and

things like that, it's really great.

And having a contact, which we had all along,
we had, you know, people could email their

questions about accessibility. Having accessibility
information available in multiple ways, for

example, for people with different sensory
disabilities you wouldn't want to have a video

that wasn't captioned explaining to people
what accessibility steps you were taking.

You'd want to have it both in writing and
and in video.

It is much more welcoming when the guidelines
are already in place. I think it was a huge

achievement that we asked presenters to agree
to follow the accessibility guidelines. They

didn't all, but it really signaled to the
community that we were going to take this

seriously.

For people that have never really thought
about accessibility and want to start thinking

about it now, could you explain what are the
issues with chat platforms in general, and

what are the points to improve?

Every commercial or even open source software
project has different accessibility issues.

I hope someday that when someone decides to
launch a product like Slack they build accessibility

into it from the ground up because it's less
work, it's more inclusive, it doesn't involve

as many hacks and things from both the programmers
and the users. With Slack, for a long time

I haven't been able to follow links if somebody
says "hey join this Zoom meeting" I can't.

I'm actually only able to use the mobile version
of Slack. For some reason the desktop and

browser versions are even worse with the screen
reader. And then there are little things in

Slack like that, depending on which update
you're on, you don't get access to the emojis

that people post in response to your messages
and stuff. And that seems really peripheral,

but when you're working fast and you're working
with a larger group of people, and you propose

to do something, and you want to know if it's
okay with everybody, a lot of the communication

happens through those emojis. So sometimes
the things that seem really peripheral are

not as peripheral as they might seem, and
they're still important to have access to.

I am also involved with the MiR Community,
minorities in R, and we have an accessibility

committee and we use Discord because it is
more accessible than Slack. But the other

day, someone sent me an invitation and I couldn't
open that one there either. So these things

are being updated all the time and it's impossible
to know which features you'll gain and lose

access to each time.

Slack had a job opening for an accessibility
person a few months ago, but the problem is

that person can't necessarily implement
what they want to implement without everyone

else agree with them. Depending on the company,
they may be in kind of a consulting role or

they may really be able to change how things
operate.

Right, that's why it's so important what you're
saying about starting to build the accessibility

features from the get-go.

Yes, yeah.

Thinking about accessibility, from the start.

Yes. And a lot of times designing things to
be accessible improves the overall design

of them. The other day I heard an explanation
of "if it's too hard to write an alt text

for your data visualization then there's probably
too much going on in your data visualization."

Same with a website. If you keep it accessible,
it will be streamlined for everybody.

As Rocío was saying in the introduction,
you were a valuable member of the organizing

team. So, as a volunteer organizer, which
moments felt most challenging, and is there

something that could have made it easier?

The most challenging moments were the weekend
before the conference started when Frans van

Dunné, Elio Campitelli, and I worked on a
chat platform for the conference. We worked

with the developers of that chat platform
- it's called The Lounge - and we worked on

making that much more screen reader accessible
and had we been able to launch it, it would

have been way more accessible than Slack.
We worked for at least nine months on this,

and when people begin to join, we found that
the software could not handle the number of

people logging on and joining multiple channels.
Frans was able to give it a really big server

to run on - it wasn't any issues like that
- there really wasn't any kind of fix for

it that we could implement in time, so we
had to go to Slack for the chat. And that

was pretty disappointing because I had already
been working on Slack for a year in preparation

for the conference and having to ask people
to email me links and things like that, so

that was the hardest part.

Despite those challenging moments, which moments
felt most rewarding of these organizer roles

with regards to conference accessibility?

One of my favorite times was seeing people
logging on from all around the world, and

joining and introducing themselves. This was
really kind of an unprecedented experience.

I don't remember how many countries were represented,
but over the process of organizing this conference,

and in the conference itself, I've been able
to meet people that I never would have been

able to meet. I haven't ever traveled to
an R conference in person, it's just not something

I usually would have the funds for. So I just
think the efforts that were made, the fact

that we had the first keynote in Spanish,
that was really great to listen to too. So

I would say it was the diversity in community.

Do you have any advice for people who have
never thought about making an accessible presentation

but would like to start?

This committee has done a terrific job of
putting together accessibility guidelines

on the website for useR!2021 it's a great
place to start because I've had a lot of links

that explain things like how to measure the
contrast and making sure that you have enough

contrast in your presentation, how to make
sure that you are not using colors that can

be easily confused by people with color blindness
and how to write good alt text. There's all

kinds of resources there. You should look
to consult with a person who actually has

the experience using those accessibility features.

Because there's really no substitute for someone
who is very familiar with using a screen reader

and you know navigating, for example, websites
or presentations. There's really no substitute

for the knowledge of people who are solving
these problems for themselves every day.

This team has been awesome and it's been really
fun meeting everyone and working with people

who have the intention of making it really
happen and doing the detailed work that has

to go into it. And I understand that getting
the captions set up and getting the captioners

into all the Zoom rooms and everything was
a really huge undertaking. I know a lot of

work was done by Joselyn and Andrea to get
me access to things in those first few days

when we realized I wouldn't have access to
them through Slack.

Unfortunately, a lot of other conferences
in my field I'm having to advocate really

hard to even have them occur on platforms
that I can get into. So, I just really appreciate

this team and this community.

Thank you so much.

Thank you Liz, it was our our pleasure to
be of help.

It was fun.

Yeah, it was. Well, thank you very much Liz
for your time. Thank you for your effort too

and in the organization. It was very valuable
to have you not only to help us make things

accessible for screen readers but also to
remind us that: it was important to have captioning

for everything, it was important to worry
about accessibility, not only for screen readers

but also for people with hearing impairments
and accessibility in terms of internet connection.

Thank you very much.

Anything else that you want to say Joselyn,
Andrea, Liz?

No, thank you so much Liz for your time and
for all your advice. I also learned a lot

about accessibility, thanks to you and your
feedback while writing the blog post at the

beginning of the organizing, so thank you
so much.

Thank you guys.
