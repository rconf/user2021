+++
title = "Tutorials at useR! 2021"
description = "What's happening at the event"
keywords = ["program","schedule","events","topics"]
+++

Every year, useR! features a tutorial program addressing the diverse interests of its audience. The tutorials are aimed at R users/developers worldwide who wish to learn about new technologies or enhance their knowledge about the existing technologies, as well as novices to the #Rstats world interested in introductory lessons. 

In the past, tutorials have helped the R user community to be updated about the latest packages, concepts, and best practices in R, and this year will not be the exception.

Since we have a global event this year, we will have a full day of tutorials in four tracks and different time zones. The 22 selected tutorials will be taught in different languages (English, Spanish or French) and they are aimed at beginner, intermediate and advanced users. **Have a look at the tutorial schedule below to learn more!** 

For a screen-reader-accessible version of this schedule, go to <a href="https://www.conftool.org/user2021/index.php?page=browseSessions&presentations=show&form_tracks=10" target="blank">our tutorial schedule on ConfTool</a>. 

**<a href="https://user2021.r-project.org/participation/registration/">Registration for the conference and for tutorials is open now</a>**. Places are limited, so don’t hesitate for too long!

<br>


{{< calendar "https://benubah.github.io/conf-schedule/useR2021/tutorial/schedule.html" >}}

