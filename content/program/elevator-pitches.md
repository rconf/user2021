+++
title = "Elevator Pitches"
description = "What Are Elevator Pitches and How They Work"
keywords = ["program","Elevator Pitch","poster session","topics"]
+++

## Instructions for Elevator Pitches Sessions

We will have two EP sessions during the conference:

EP1: **Tuesday July 6 12:45 am - 2:15 am UTC**<br>
EP2: **Tuesday July 6 1 pm - 2:30 pm UTC**


## What are EPs?

They are a new format that replaces the regular poster session. Each EP presentation is either in the form of a lightning talk of ~5 minutes, or a technical note that should be read in ~5 minutes. 

## Where do we find the EPs?

Each EP has a dedicated channel. The links to the videos and technical notes are in the topics and descriptions of these channels, which start with “EP_” in the name. 

**Here is an overview of all <a href="/participation/technical_notes/overview">technical notes</a>**

There is also a main elevator_pitches channel. Here, we will publish the list of EPs for each of the two sessions, with the names of the channels, the title of their presentations, and links to the material.



## How can I communicate with the authors?

That’s what the EP sessions are for! The authors will be waiting for you in their corresponding channel, so please go chat with them. If a call is better than written communication, it is possible to start a call on Slack in the channels. 

What if the EP session is not in a favorable time zone for me, could I still access the material?
Yes, this material is accessible throughout the week. And you can still go visit the authors’ channels during the week. They just may not be as quick to interact with you as during their session, but please feel free to pay them a visit anyway.


## Could I just check the material and not talk to the authors?

Yes, you could. But it would be nice to go visit them, even if you only go to the channel to say that you liked the video/note. The R community is so great because we support each other :) 

## What should I do as an EP presenter?

When the session starts, you should go and find your channel. Make sure that the link in the channel description is working. You could also start a call in slack that people can join when they enter your channel. To do so, click on the channel members icon at the top right. Then click “Start Call” without adding a specific person. This will post the call to your channel and others can join it. 

<br><br>