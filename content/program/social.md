+++
title = "Social Events"
description = "From a movie screening to a cartoon artwork tutorial and Yoga sessions. Be a part of useR!2021 beyond R."
keywords = ["program","social","events","social event"]
+++


## useR! beyond R

Take part in our social events from a movie screening, to rechaRge (our yoga sessions) and a cartoon workshop. Looking forward to welcoming you! 

## To get Started


<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">Welcome and Navigation Guide</div>
            <div class="slide-text">
            <div class="text-box">
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Sunday July 4, 2021, 10:15pm UTC    <br>
                Monday July 5, 2021, 07:15am UTC   <br>
                Monday July 5, 2021, 01:15pm UTC  
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot.png" height="230">
            </div>
        </div>
</div>
</div>


<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">My first useR! (session 1)</div>
            <div class="slide-text">
            <div class="text-box-intro">Pre-conference session aimed at newcomers to useR! as an introduction on how to engage with the wider R community. This session will be interactive with open discussion and a chance for networking.
            </div>
            <div class="guest">
                <img src="/img/artwork/yihui.png" width="100"><div class="intro-guest-text"><b>My Journey in blogging and Writing books about R</b><br>
                Yihui Xie, Rstudio</div>
                <img src="/img/artwork/malcolm.png" width="100"><div class="intro-guest-text"><b>The Gateless Gate: Contributing to R Packages</b><br>Malcolm Barrett, Teladoc Health</div>
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Sunday July 4, 2021, 23:15:00 UTC
            </div>
        </div>
</div>
</div>


<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">My first useR! (session 2)</div>
            <div class="slide-text">
            <div class="text-box-intro">Pre-conference session aimed at newcomers to useR! as an introduction on how to engage with the wider R community. This session will be interactive with open discussion and a chance for networking
            </div>
              <div class="guest">
                <img src="/img/artwork/maelle.png" width="100"><div class="intro-guest-text"><b>Find your way in the R world</b><br>
                Maëlle Salmon, R(esearch) Software Engineer</div>
                <img src="/img/artwork/paula.png" width="100"><div class="intro-guest-text"><b>R for spatial data analysis and viz</b><br>Paula Moraga, Assistant Professor of Statistics</div>
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Monday July 5, 2021, 14:15:00 UTC
            </div>
        </div>
</div>
</div>


## Monday

<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">RechaRge</div>
            <div class="slide-text">
            <div class="text-box">Yoga Intro + Breathing exercises: Jana will teach us how to breath and will lead a yoga class open to all. Come join us! Beginners are welcome.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Monday July 5, 2021, 23:30:00 UTC
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot-yoga.png" height="230">
            </div>
        </div>
</div>
</div>


<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">MiR community event</div>
            <div class="slide-text">
            <div class="text-box">During this event we plan to provide a space of community and gathering for current members, highlight our community and what we offer to prospective members, and have a “state of MiR” presentation in which we talk about our strides, what we still need to work on, and future directions.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Monday July 5, 2021, 23:30:00 UTC
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot-mir.png" height="230">
            </div>
        </div>
</div>
</div>


## Tuesday

<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">mixR!</div>
            <div class="slide-text">
            <div class="text-box">Music, networking channel and raffles. Let’s end the day in a relaxing way.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Tuesday July 6, 2021, 02:15:00 UTC
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot-diversity.png" height="230">
            </div>
        </div>
</div>
</div>


<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">Creation Lab</div>
            <div class="slide-text">
            <div class="text-box">Want to learn how to draw cartoons on your tablet or with pen and pencil? come join us! Anali will teach the basics on how to make cartoons, and tell stories with drawings.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Tuesday July 6, 2021, 06:30 UTC
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot-artist.png" height="230">
            </div>
        </div>
</div>
</div>


<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">RechaRge</div>
            <div class="slide-text">
            <div class="text-box">Yoga for Calm + Meditation: Jana will teach us how to relax with yoga and will lead a couple of meditation techniques. Come join us! Beginners are welcome.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Tuesday July 6, 2021, 10:15 UTC
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot-yoga.png" height="230">
            </div>
        </div>
</div>
</div>


<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">R-Ladies community event</div>
            <div class="slide-text">
            <div class="text-box">Come meet R-Ladies from all over the world and share local experiences globally.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Tuesday July 6, 2021, 10:15 UTC
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot-rladies-lookout.png" height="230">
            </div>
        </div>
</div>
</div>


<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">mixR!</div>
            <div class="slide-text">
            <div class="text-box">Music, networking channel and raffles. Let’s end the day in a relaxing way.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Tuesday July 6, 2021, 17:00:00 UTC
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot-diversity.png" height="230">
            </div>
        </div>
</div>
</div>


## Wednesday

<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">Movie: Coded Bias</div>
            <div class="slide-text">
            <div class="text-box">When MIT Media Lab researcher Joy Buolamwini discovers that many facial recognition technologies misclassify women and darker-skinned faces, she delves into an investigation of widespread bias in algorithms.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">                
            US/UK/China/South Africa | 2020 |<br> 90 minutes | Color | CC<br>
                Wednesday July 7, ALL DAY<br>
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot-popcorn.png" height="230">
            </div>
        </div>
</div>
</div>

## Thursday


<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">aRt Gallery</div>
            <div class="slide-text">
            <div class="text-box">Step in the virtual gallery with aRt from the amazing Danielle Navarro, Ijeamaka Anyene, Will Chase, Thomas Lin Petersen, and Antonio Sanchez. Come hear from these fabulous artists and see their artwork.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Thursday July 8, 2021, 04:30 UTC<br>
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/art.png" width="200">
            </div>
        </div>
</div>
</div>


<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">RechaRge</div>
            <div class="slide-text">
            <div class="text-box">Yoga for the Spine + Stretching: Jana will teach us how to stretch our spines and offer a bit of help for back pain. Come join us! Beginners are welcome.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Thursday July 8, 2021, 08:15:00 UTC
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot-yoga.png" height="230">
            </div>
        </div>
</div>
</div>




<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">mixR!</div>
            <div class="slide-text">
            <div class="text-box">Music, networking channel and raffles. Let’s end the day in a relaxing way.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Thursday July 8, 2021 at 10:45 UTC
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot-diversity.png" height="230">
            </div>
        </div>
</div>
</div>

## Friday

<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">tRivia</div>
            <div class="slide-text">
            <div class="text-box">
Come spend some time meeting R users from around the world while testing your trivia skills about technology and the R language! Participants will be asked to team up to see who scores the most points to win a very special pRize!
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Friday July 9, 2021, 15:15 UTC
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot.png" height="230">
            </div>
        </div>
</div>
</div>

<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">RechaRge</div>
            <div class="slide-text">
            <div class="text-box">Chair Yoga: Jana will teach us how to stretch our bodies using a chair. Come join us to have a great end of the week workout. Beginners are welcome.
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Friday July 9, 2021 at 17:45 UTC
            </div>
            <div class="slide-marmot">
            <img src="/img/artwork/marmot-yoga.png" height="230">
            </div>
        </div>
</div>
</div>


<div class="row social">
<div class="col-6">
        <div class="slide">
            <div class="slide-headline"><img class="slide-logo" src="/img/artwork/user-logo-negative.png" width="90">Awards and Closing Ceremony</div>
            <div class="slide-text">
            <div>
                <img src="/img/artwork/marmot.png" height="100"><img src="/img/artwork/marmot-rladies.png" height="100"><img src="/img/artwork/marmot-africar.png" height="100"><img src="/img/artwork/marmot-mir.png" height="100"><img src="/img/artwork/marmot-diversity.png" height="100">
            </div>
            </div>
            <div class="slide-line"></div>
            <div class="slide-footer">
                Friday July 9, 2021, 21:30 UTC<br>              
            </div>
            <div class="slide-marmot">
            </div>
        </div>
</div>
</div>
