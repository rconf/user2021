+++
title = "Watch Recordings"
description = "Watch recorded useR! 2021 talks, elevator pitches and keynotes."
keywords = ["Rstats","programming","conference","week of R"]
+++


## Recordings of Keynotes

[YouTube playlist of useR! 2021 Keynotes](https://youtube.com/playlist?list=PL4IzsxWztPdmHoJwIVa4um44w2GMjctmP)

## Recordings of Talks (Regular Sessions) 

[YouTube playlist of useR! 2021 Talks](https://youtube.com/playlist?list=PL4IzsxWztPdmHhzrXDAOpq4zS_peAVty2)

## Recordings of Tutorials

[YouTube playlist of useR! 2021 Tutorials](https://youtube.com/playlist?list=PL4IzsxWztPdnCC_kMCYKrd_t6cViMhBrD)


## Recordings of Elevator Pitches

Instead of a traditional poster session useR! 2021 gave participants the opportunity to pitch their project in a short video or a written technical note. 

[YouTube playlist of useR! 2021 Elevator Pitches](https://youtube.com/playlist?list=PL4IzsxWztPdnviiir8c5GaarXd9vZ2TIP)

[Technical notes of useR! 2021](https://user2021.r-project.org/participation/technical_notes/overview/)

<br>
<br><br>


